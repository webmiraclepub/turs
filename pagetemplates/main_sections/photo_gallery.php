<section id="photo-gallery">
	<div class="section-title">
		<h2>Галерея</h2>
		<div class="section-title__line section-title__line_blue"></div>
		<img class="abs-image-left hide-for-small-only" data-src="<?= $img_oblako[0] ?>">
		<img class="abs-image-left  hide-for-small-only" data-src="<?= $img_oblako[1] ?>">
		<img class="abs-image-left  hide-for-small-only" data-src="<?= $img_oblako[2] ?>">
		<img class="abs-image-right  hide-for-small-only" data-src="<?= $img_plain ?>">
		<img class="abs-image-right  hide-for-small-only" data-src="<?= $img_oblako[0] ?>">
		<img class="abs-image-right  hide-for-small-only" data-src="<?= $img_oblako[1] ?>">
		<img class="abs-image-right  hide-for-small-only" data-src="<?= $img_oblako[2] ?>">
	</div>
	<div id="photo-gallery-card">
		<div class="grid-container">
			<?php miracle_get_gallery_card( 3 ); ?>
		</div>
	</div>
	<button id="add_new_gallery_posts" class="button expanded">Смотреть еще</button>
</section>