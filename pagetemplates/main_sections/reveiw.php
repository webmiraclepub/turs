<section id="reveiw">
	<div class="bg-krugi hide-for-small-only"></div>
	<div class="bg-krugi2 hide-for-small-only"></div>
	<h2 class="section-title"><?= $title ?></h2>
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="small-12 cell">
				<div
					class="owl-carousel owl-theme reveiw-carousel"
					data-animate="fadeIn"
					data-animate-delay="0.5s"
					data-animate-duration="1s">
					<?php miracle_get_reveiw_item(); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="bottom-bg"></div>
</section>