<?php
	$title     = "Вопросы";
	$img_left  = get_template_directory_uri() . '/dist/assets/images/stars.png';
	$img_right = get_template_directory_uri() . '/dist/assets/images/chemodan.svg';
?>

<section id="question">
	<h2 class="section-title"><?= $title ?></h2>
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="small-12 cell">
				<ul class="accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true">
					<?php miracle_get_question(); ?>
				</ul>
			</div>
			<div 
				class="small-12 medium-8 float-center cell form">
				<p class="text-center mobile_before_footer_form_title">Я еду!</p>
				<?php miracle_get_form(); ?>
			</div>
		</div>
	</div>
	<img src="<?= $img_left ?>" class="abs-image-left hide-for-small-only">
	<img src="<?= $img_right ?>" class="abs-image-right hide-for-small-only">
</section>