<section id="comment">
	<h2 class="section-title"><?= $title ?></h2>
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell small-12">
				<div
					class="owl-carousel owl-theme comment-carousel"
					data-animate="fadeInUp"
					data-animate-delay="0.5s"
					data-animate-duration="1s">
					<?php miracle_get_comment_card(); ?>
				</div>
			</div>
		</div>
	</div>
</section>