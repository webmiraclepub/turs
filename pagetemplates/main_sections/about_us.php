<section id="about-us">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="cell">
				<h2 class="text-center section-title"><?= $title ?></h2>
			</div>
		</div>
	</div>
	<div class="grid-container">
		<div class="grid-x grid-padding-x grid-padding-y small-up-1 medium-up-2 large-up-4">
			<?php miracle_get_about_us_card(); ?>
		</div>
	</div>
</section>