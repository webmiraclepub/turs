<?php
/*
Template Name: Страница "Главная страница"
*/

	get_header();

	// include( get_template_directory() . '/pagetemplates/main_sections/mobile/after_header_form.php' );

	$title = get_field('miracle_about_title');
	include( get_template_directory() . '/pagetemplates/main_sections/about_us.php');

	$title = get_field( 'miracle_working_title');
	include( get_template_directory() . '/pagetemplates/main_sections/working.php');

	$title = get_field( 'miracle_tours_title' );
	global $slug;
	$num_month = date('n');
	$num_season = 12 / $num_month;
	if ($num_season > 1 && $num_season <= 1.4){
		$current_season = 'Осень';
		$months = 'сентябрь/октябрь/ноябрь';
		$slug = 'autumn';
	} elseif ($num_season >= 1.5 && $num_season <= 2) {
		$current_season = 'Лето';
		$months = 'июнь/июль/август';
		$slug = 'summer';
	} elseif ($num_season >=  2.4 && $num_season <= 4){
		$current_season = 'Весна';
		$months = 'март/апрель/май';
		$slug = 'spring';
	} else {
		$current_season = 'Зима';
		$months = 'декабрь/январь/февраль';
		$slug = 'winter';
	}

	$all_tours = get_field( 'miracle_tours_active_posts' );
	$count_tours = 0;
	foreach ($all_tours as $tour):
		$taxonomy = get_field( 'miracle_tour_post_taxonomy', $tour->ID );
		if( $taxonomy->slug == $slug ):
			$count_tours ++;
		endif;
	endforeach;

	if( $count_tours == 0 ){
		$count_tours = 'доступных туров на данный момент не организовано';
	}elseif( $count_tours == 1 ){
		$count_tours = 'доступен ' . $count_tours . ' тур';
	}elseif( $count_tours > 1 && $count_tours < 5 ){
		$count_tours = 'доступно ' . $count_tours . ' тура';
	}else{
		$count_tours = 'доступно ' . $count_tours . ' туров';
	}
	include( get_template_directory() . '/pagetemplates/main_sections/tour.php');

	$img_oblako = array();
	$img_oblako[0] = get_template_directory_uri() . '/dist/assets/images/oblako1.svg';
	$img_oblako[1] = get_template_directory_uri() . '/dist/assets/images/oblako2.svg';
	$img_oblako[2] = get_template_directory_uri() . '/dist/assets/images/oblako3.svg';
	$img_plain = get_template_directory_uri() . '/dist/assets/images/samolet.svg';
	$img1 = get_template_directory_uri() . '/dist/assets/images/gori.png';
	include( get_template_directory() . '/pagetemplates/main_sections/photo_gallery.php');

	$title = get_field( 'miracle_videotchet_section_title' );
	include( get_template_directory() . '/pagetemplates/main_sections/reveiw.php');
	
	$title = get_field( 'miracle_reviews_section_title' );
	include( get_template_directory() . '/pagetemplates/main_sections/comment.php');

	get_footer();
?>
