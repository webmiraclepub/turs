<?php

$logfile = get_stylesheet_directory().'/log.txt';
$preservelog = true;

include('lib/init.php');
include('lib/ajax.php');


register_nav_menu( 'main', 'Главное меню' );


/* PTE and Yoast SEO conflict fix. https://wordpress.org/support/topic/js-error-and-upload-failed */
remove_action('dbx_post_advanced', 'pte_edit_form_hook_redirect');

function my_myme_types($mime_types){
    $mime_types['svg'] = 'image/svg+xml'; // поддержка SVG
    return $mime_types;
}
add_filter('upload_mimes', 'my_myme_types', 1, 1);


add_action('save_post', 'nov_change_title');

function nov_change_title($post_id) {
    if (get_post_type() == 'tour-gallery'):

        // get files to compress
        $gallery_object = get_field_object('mitacle_tour_gallery_post');
        foreach ($gallery_object['value'] as $image) {
            $files[] = $image['url'];
        }

        // work with zip object
        $zip = new ZipArchive();
        $dir = dirname( __FILE__ );
        if ( !file_exists ( $dir.'/zip' ) )
            mkdir( $dir.'/zip' );
        $filename = $dir."/zip/archive_".$post_id.".zip";
        $create = $zip->open( $filename, ZipArchive::CREATE );
        if ( $create!==TRUE ) {
            exit( "Невозможно открыть <$filename>\n" );
        }

        // prepare correct directory
        $home = __FILE__;
        $final = '';
        $parts = explode( '/', $home );
        foreach ( $parts as $part ) {
            if ( $part != 'wp-content' ){
                if ( $part == '' )
                    continue;
                $final .= '/'.$part;
            }
            else{
                break;
            }
        }

        foreach ( $files as $file ) {
            $file = parse_url( $file, PHP_URL_PATH );
            $add = $zip->addFile( $final.$file, basename( $file ) );
        }

        $exit = $zip->close();

    endif;
}  


// Отключаем сам REST API
add_filter('rest_enabled', '__return_false');
 
// Отключаем фильтры REST API
remove_action( 'xmlrpc_rsd_apis', 'rest_output_rsd' );
remove_action( 'wp_head', 'rest_output_link_wp_head', 10, 0 );
remove_action( 'template_redirect', 'rest_output_link_header', 11, 0 );
remove_action( 'auth_cookie_malformed', 'rest_cookie_collect_status' );
remove_action( 'auth_cookie_expired', 'rest_cookie_collect_status' );
remove_action( 'auth_cookie_bad_username', 'rest_cookie_collect_status' );
remove_action( 'auth_cookie_bad_hash', 'rest_cookie_collect_status' );
remove_action( 'auth_cookie_valid', 'rest_cookie_collect_status' );
remove_filter( 'rest_authentication_errors', 'rest_cookie_check_errors', 100 );
 
// Отключаем события REST API
remove_action( 'init', 'rest_api_init' );
remove_action( 'rest_api_init', 'rest_api_default_filters', 10, 1 );
remove_action( 'parse_request', 'rest_api_loaded' );
 
// Отключаем Embeds связанные с REST API
remove_action( 'rest_api_init', 'wp_oembed_register_route');
remove_filter( 'rest_pre_serve_request', '_oembed_rest_pre_serve_request', 10, 4 );
 
remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );