<?php
	$logo = get_field( 'miracle_global_site_logo', 'option' );
	$logo_img = $logo['url'];
	$logo_alt = $logo['alt'];
	$logo_title = $logo['description'];
	$viber_phone = get_field('miracle_option_phone_viber', 'option');
	$fenix_phone = get_field('miracle_option_phone_fenix', 'option');
	global $post;
	$ID = $post->ID;
	$bgi = ( is_single() ) ? get_the_post_thumbnail_url( $ID, 'full' ) : get_field('miracle-global-header-image', 'option');
	$subtitle = ( is_single() ) ? get_field('miracle_post_subtitle') : get_theme_mod('miracle_global_header_subtitle', '');
	$title = ( is_single() ) ? get_the_title() : get_theme_mod('miracle_global_header_title', '');
?>

	<body <?php body_class($class); ?>>
		<!--[if lt IE 7]>
			<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
		<![endif]-->

<?php
  get_sidebar( 'mobile' );
?>

<div class="off-canvas-content" data-off-canvas-content>

<header id="header">
	<div class="hide-for-large" data-sticky-container>
		<div class="sticky" data-sticky data-margin-top="0" data-sticky-on="small">
			<div class="title-bar" data-toggle="offCanvas" data-hide-for="large">
				<button class="menu-icon" type="button" data-toggle="example-menu"></button>
				<div class="title-bar-title">
					<img src="<?= $logo_img ?>" alt="<?= $logo_alt ?>" title="<?= $logo_title ?>">
				</div>
			</div>
		</div>
	</div>
	<div id="top-logo" class="grid-container">
		<div class="show-for-large grid-x">
			<div class="for-logo">
				<div class="cell">
					<img id="site-logo" data-animate-logo src="<?= $logo_img ?>" alt="<?= $logo_alt ?>" title="<?= $logo_title ?>">
				</div>
			</div>
			<div class="header-phone vertical-align">
				<p  data-animate="flipInX" data-animate-delay="0.5s" data-animate-duration="1s">
					<i class="miracle-icon-viber"></i><?= $viber_phone ?>
				</p>
				<p  data-animate="flipInX" data-animate-delay="0.5s" data-animate-duration="1s">
					<i class="miracle-icon-watsup"></i><?= $fenix_phone ?>
				</p>
			</div>
		</div>
	</div>
	<div id="header-title" style="background-image: url(<?= $bgi ?>)">
		<div class="grid-container">
			<div class="grid-x text-center vertical-align">
				<?php if( !is_single( ) ): ?>
				<div class="cell small-12 medium-10 medium-offset-1 large-6 large-offset-3 hide-for-small-only">
					<p class="subtitle" data-animate="fadeInUpBig" data-animate-delay="0.2s" data-animate-duration="1s">
						<?= $subtitle ?>
					</p>
				</div>
				<?php endif; ?>
				<div class="cell small-12 large-10 large-offset-1">
					<h1 data-animate="fadeInUpBig" data-animate-delay="0s" data-animate-duration="1s"><?= $title ?></h1>
				</div>
			</div>
		</div>
	</div>
</header>

  <?php miracle_get_sidebar(); ?>