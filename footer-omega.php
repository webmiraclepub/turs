		<div><!-- End mobile menu -->
		<?php wp_footer(); ?>
		<script type="text/javascript">
		<?php
		global $active_tour_ids; 
		global $program_tour_ids;
		foreach ($active_tour_ids as $id) { ?>
			$('#tour .owl-carousel.tour_gallery_slider_<?php echo $id; ?>').owlCarousel({
	            margin:0,
	            stagePadding:0,
	            nav:false,
	            dots:true,
	            loop:true,
	            callbacks:true,
	            lazyLoad:true,
	            mouseDrag:false,
	            autoplay:true,
	            animateIn:'fadeIn',
	            animateOut:'fadeOut',
	            items:1
	        });
		<?php } 
		foreach ($program_tour_ids as $id) { ?>
			$('#owl-program-<?php echo $id; ?>').owlCarousel({
	            margin:0,
	            stagePadding:0,
	            nav:true,
	            dots:false,
	            loop:false,
	            callbacks:true,
	            lazyLoad:true,
	            touchDrag: false,
	            navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
	            items:1
		    });
		<?php } ?>
		</script>
		<?php if (defined('DEVMODE')): ?>
			<script src="//localhost:35729/livereload.js"></script>
		<?php endif; ?>
	</body>
</html>
