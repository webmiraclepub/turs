<div class="show-for-large" id="for-stiky-menu" data-sticky-container>
  <nav id="navigation" data-sticky data-options="marginTop:0;"  data-top-anchor="for-stiky-menu">
    <div class="grid-container">
      <div class="cell menu-centered">
        <?php miracle_get_menu_item( 'expanded' ); ?>
      </div>
    </div>
  </nav>
</div>