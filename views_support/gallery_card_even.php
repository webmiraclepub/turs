<div
	class="grid-x gallery"
	data-animate="fadeInRight"
	data-animate-delay="0.5s"
	data-animate-duration="1s">
	<div class="small-image medium-5 hide-for-small-only cell" data-radio="height" data-radio-size="1.6666666">
		<img data-src="<?= $img[0]['sizes']['thumbnail'] ?>" alt="<?= $img[0]['alt'] ?>" title="<?= $img[0]['title'] ?>">
		<img data-src="<?= $img[1]['sizes']['thumbnail'] ?>" alt="<?= $img[1]['alt'] ?>" title="<?= $img[1]['title'] ?>">
		<img data-src="<?= $img[2]['sizes']['thumbnail'] ?>" alt="<?= $img[2]['alt'] ?>" title="<?= $img[2]['title'] ?>">
		<img data-src="<?= $img[3]['sizes']['thumbnail'] ?>" alt="<?= $img[3]['alt'] ?>" title="<?= $img[3]['title'] ?>">
		<img data-src="<?= $img[4]['sizes']['thumbnail'] ?>" alt="<?= $img[4]['alt'] ?>" title="<?= $img[4]['title'] ?>">
	</div>
	<div class="medium-image small-6 medium-3 cell" data-radio="height" data-radio-size="1">
		<img data-src="<?= $img[5]['sizes']['thumbnail'] ?>" alt="<?= $img[5]['alt'] ?>" title="<?= $img[5]['title'] ?>">
		<img data-src="<?= $img[6]['sizes']['thumbnail'] ?>" alt="<?= $img[6]['alt'] ?>" title="<?= $img[6]['title'] ?>">
	</div>
	<div class="index-image small-6 medium-4 cell" data-radio="height" data-radio-size="1.3333333">
		<img data-src="<?= $index_image ?>" alt="<?= $title ?>" title="<?= $title ?>">
		<div class="info-img">
			<div class="vertical-align">
				<h2><?= $title ?> <span><?= $year ?></span></h2>
				<p>(<?= $count ?> фото)</p>
				<a href="<?= $download ?>" download class="button yellow">Скачать архив</a>
			</div>
		</div>
	</div>
</div>