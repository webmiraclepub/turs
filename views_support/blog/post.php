<div
	class="cell post lazy"
	data-radio="height"
	data-radio-size="2"
	data-src="<?= $img ?>"
	data-animate="<?= $animation ?>"
	data-animate-delay="1s"
	data-animate-duration="1s">
	<div class="vertical-align">
		<h2 class="post-title"><?= $title ?></h2>
		<p><?= $content ?></p>
		<a href="<?= $url ?>" class="button yellow">подробнее</a>
	</div>
</div>