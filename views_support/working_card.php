<div class="item">
	<div class="card">
		<div class="card-section image">
			<div
				class="width-100"
				data-radio="height"
				data-radio-size="1"
				data-animate="flipInY"
				data-animate-delay="0.5s"
				data-animate-duration="1s">
				<img class="owl-lazy" data-src="<?= $image ?>" alt="<?= $title ?>" title="<?= $title ?>">
			</div>
		</div>
		<div class="card-section" data-equalizer-watch="work-pavel">
			<h3><?= $title ?></h3>
			<p><?= $status ?></p>
			<ul class="menu" data-animate="bounce" data-animate-delay="1.5s" data-animate-duration="0.75s">
				<?php miracle_get_working_social_icons( $icons ); ?>
			</ul>
		</div>
	</div>
</div>