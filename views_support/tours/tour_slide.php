<div class="item" data-equalizer-watch="owl-carousel-<?= $id ?>" data-tour-day="<?= $day ?>" data-tour-content="<?= $content ?>">
	<img class="owl-lazy" data-src="<?= $url ?>" alt="<?= $alt ?>" title="<?= $title ?>">
</div>