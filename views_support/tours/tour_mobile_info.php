<div class="text-center mobile_tour_card" class="owl-item">
	<h2 class="mobile_tour_card__header"><?= $title ?></h2>
	<small><?= $data_on ?>-<?= $data_off ?></small>
	<div class="form mobile_tour_info">
		<h3 class="mobile_tour_card__subtitle">Программа тура</h3>
		<div class="owl-carousel program-carousel owl-theme" id="owl-program-<?= $id ?>">
			<?php miracle_get_tour_slider( $id ); ?>
		</div>
		<div class="after-tour-slider">
			<?php miracle_get_tour_slide_info( $id ); ?>
		</div>
		<h3 class="mobile_tour_card__subtitle">Я еду!</h3>
		<?php miracle_get_form( true, $title ); ?>
		<h3 class="mobile_tour_card__subtitle">Включено</h3>
		<div class="grid-container include">
			<div class="grid-x grid-padding-x small-up-2 medium-up-4">
				<?php miracle_get_tour_includes( $id ); ?>
			</div>
		</div>
	</div>
</div>