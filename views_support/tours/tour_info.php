<tr>
</tr>

<tr>
	<td class="text-center show-for-medium"><?= $data_on ?>-<?= $data_off ?></td>
	<td class="text-left show-for-medium"><?= $title ?></td>
	<td class="show-for-medium"><?= $fishka ?></td>
	<td class="show-for-medium">
		<p class="tour_price">
			<span>
				<?= $new_price ?>р. <span><?= $old_price ?>р.</span>
			</span>
		</p>
	</td>
	<td class="show-for-medium"><?= $count ?></td>
	<td class="show-for-medium">
		<button class="button <?php echo $active; ?>" data-toggle="<?= $toggler ?>">Подробнее</button>
	</td>

</tr>
<tr class="hide animated" id="<?= $toggler ?>" data-toggler=".hide bounceInUp">
	<td colspan="100%">
		<div class="grid-container">
			<div class="grid-x grid-padding-x">
				<div class="cell small-12 large-6 program-section">
					<h3 class="right-title">Программа тура</h3>
					<div class="owl-carousel owl-theme program-carousel" data-equalizer="owl-carousel-<?= $id ?>">
						<?php miracle_get_tour_slider( $id ); ?>
					</div>
					<div class="after-tour-slider">
						<?php miracle_get_tour_slide_info( $id ); ?>
					</div>
				</div>
				<div class="cell small-12 large-6 left-tour-info">
					<h3 class="left-title">Я еду!</h3>
					<?php miracle_get_form( true, $title ); ?>
					<h3 class="right-title text-center">Включено</h3>
					<div class="grid-container include">
						<div class="grid-x grid-padding-x small-up-2 medium-up-4">
							<?php miracle_get_tour_includes( $id ); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="grid-container show-for-medium">
			<div class="owl-carousel owl-theme tour_gallery_slider<?php echo '_'.$id ?>">
				<?php miracle_add_tour_gallery_carousel( $id ); ?>
			</div>
		</div>
	</td>
</tr>