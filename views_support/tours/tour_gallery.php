<div
	class="grid-x gallery item show-for-large">
	<div class="index-image small-4 cell" data-radio="height" data-radio-size="1.3333333">
		<img class="owl-lazy" data-src="<?= $index_image ?>" alt="<?= $title ?>" title="<?= $title ?>">
		<div class="info-img">
			<div class="vertical-align">
				<h2><?= $title ?> (<?= $year ?>)</h2>
				<h2><span>Фотоотчет</span></h2>
			</div>
		</div>
	</div>
	<div class="medium-image small-3 cell" data-radio="height" data-radio-size="1">
		<a class="fancybox-thumb" rel="fancybox-thumb-<?= $id ?>" href="<?= $img[0]['url'] ?>" title="<?= $img[0]['alt'] ?>">
			<img class="owl-lazy" data-src="<?= $img[0]['sizes']['thumbnail'] ?>" alt="<?= $img[0]['alt'] ?>" title="<?= $img[0]['title'] ?>">
		</a>
		<a class="fancybox-thumb" rel="fancybox-thumb-<?= $id ?>" href="<?= $img[1]['url'] ?>" title="<?= $img[1]['alt'] ?>">
			<img class="owl-lazy" data-src="<?= $img[1]['sizes']['thumbnail'] ?>" alt="<?= $img[1]['alt'] ?>" title="<?= $img[1]['title'] ?>">
		</a>
	</div>
	<div class="small-image small-5 cell" data-radio="height" data-radio-size="1.6666666">
		<a class="fancybox-thumb" rel="fancybox-thumb-<?= $id ?>" href="<?= $img[2]['url'] ?>" title="<?= $img[2]['alt'] ?>">
			<img class="owl-lazy" data-src="<?= $img[2]['sizes']['thumbnail'] ?>" alt="<?= $img[2]['alt'] ?>" title="<?= $img[2]['title'] ?>">
		</a>
		<a class="fancybox-thumb" rel="fancybox-thumb-<?= $id ?>" href="<?= $img[3]['url'] ?>" title="<?= $img[3]['alt'] ?>">
			<img class="owl-lazy" data-src="<?= $img[3]['sizes']['thumbnail'] ?>" alt="<?= $img[3]['alt'] ?>" title="<?= $img[3]['title'] ?>">
		</a>
		<a class="fancybox-thumb" rel="fancybox-thumb-<?= $id ?>" href="<?= $img[4]['url'] ?>" title="<?= $img[4]['alt'] ?>">
			<img class="owl-lazy" data-src="<?= $img[4]['sizes']['thumbnail'] ?>" alt="<?= $img[4]['alt'] ?>" title="<?= $img[4]['title'] ?>">
		</a>
		<a class="fancybox-thumb" rel="fancybox-thumb-<?= $id ?>" href="<?= $img[5]['url'] ?>" title="<?= $img[5]['alt'] ?>">
			<img class="owl-lazy" data-src="<?= $img[5]['sizes']['thumbnail'] ?>" alt="<?= $img[5]['alt'] ?>" title="<?= $img[5]['title'] ?>">
		</a>
		<a class="fancybox-thumb" rel="fancybox-thumb-<?= $id ?>" href="<?= $img[6]['url'] ?>" title="<?= $img[6]['alt'] ?>">
			<img class="owl-lazy" data-src="<?= $img[6]['sizes']['thumbnail'] ?>" alt="<?= $img[6]['alt'] ?>" title="<?= $img[6]['title'] ?>">
		</a>
	</div>
</div>