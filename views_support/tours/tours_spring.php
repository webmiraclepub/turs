<?php global $slug; ?>
<div class="tabs-panel <?php if ($slug == 'spring') echo 'is-active' ?>" id="spring_tours" data-count="<?= count($spring_tours); ?>">
	<table class="show-for-medium" cellspacing="0">
		<thead>
			<tr>
				<th class="text-center show-for-medium">дата</th>
				<th class="text-left show-for-medium">тур</th>
				<th class="show-for-medium">фишка тура</th>
				<th class="show-for-medium">цена</th>
				<th class="show-for-medium">кол-во мест</th>
				<th class="show-for-medium"></th>
			</tr>
		</thead>
		<tbody>
			<?php miracle_add_tour_info( $spring_tours ); ?>
		</tbody>
	</table>
	<div class="show-for-small-only owl-carousel owl-theme" id="owl-spring">
		<?php miracle_mobile_tour_info( $spring_tours ); ?>
	</div>
</div>