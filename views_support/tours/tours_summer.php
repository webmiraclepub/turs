<?php global $slug; ?>
<div class="tabs-panel <?php if ($slug == 'summer') echo 'is-active' ?>" id="summer_tours" data-count="<?= count($summer_tours); ?>">
	<table class="show-for-medium" cellspacing="0">
		<thead>
			<tr>
				<th class="text-center show-for-medium">дата</th>
				<th class="text-left show-for-medium">тур</th>
				<th class="show-for-medium">фишка тура</th>
				<th class="show-for-medium">цена</th>
				<th class="show-for-medium">кол-во мест</th>
				<th class="show-for-medium"></th>
			</tr>
		</thead>
		<tbody>
			<?php miracle_add_tour_info( $summer_tours ); ?>
		</tbody>
	</table>
	<div class="show-for-small-only owl-carousel owl-theme" id="owl-summer">
		<?php miracle_mobile_tour_info( $summer_tours ); ?>
	</div>
</div>