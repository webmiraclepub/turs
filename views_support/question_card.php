<li
	class="accordion-item"
	data-accordion-item
	data-animate="flipInX"
	data-animate-delay="0.5s"
	data-animate-duration="1s">
	<a href="#" class="accordion-title"><?= $quest ?></a>
	<div class="accordion-content" data-tab-content >
		<p><?= $answer ?></p>
	</div>
</li>