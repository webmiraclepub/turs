<section id="question">
  <div class="grid-container">
    <div class="grid-x grid-padding-x">
      <div 
	    class="small-12 medium-8 medium-offset-2 cell form"
		data-animate="tada"
		data-animate-delay="1s"
		data-animate-duration="1s">
        <?php miracle_get_form(); ?>
      </div>
    </div>
  </div>
  <img data-src="<?= $img_left ?>" class="hide-for-small-only abs-image-left">
  <img data-src="<?= $img_right ?>" class="hide-for-small-only abs-image-right">
</section>