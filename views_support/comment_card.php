<div class="item comment-carousel-item">
	<div class="content">
		<!-- <div class="pre-image"> -->
			<div class="image" data-radio="height" data-radio-size="1">
				<img class="owl-lazy" data-src="<?= $img ?>" alt="<?= $title ?>" title="<?= $title ?>">
			</div>
		<!-- </div> -->
		<h5 class="date show-for-small-only"><?= $title ?><br><span><?= $status ?></span></h5>
		<div class="content-text" data-radio="height" data-radio-size="3">
			<p><?= $content ?></p>
		</div>
	</div>
	<h5 class="date show-for-medium"><?= $title ?><br><span><?= $status ?></span></h5>
</div>