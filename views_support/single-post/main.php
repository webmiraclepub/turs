<section id="single-post">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell post-content">
				<?= $content ?>
			</div>
			<div class="cell text-center">
				<a href="<?= $url_blog ?>" class="button orange in-blog">Вернуться ко всем статьям</a>
			</div>
		</div>
	</div>
</section>