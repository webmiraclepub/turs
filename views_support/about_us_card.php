<div class="cell info-card">
	<p data-number="<?= $card['intager'] ?>" data-animate-number="tada"><?= $card['intager'] ?></p>
	<p data-animate="fadeInDown" data-animate-delay="0s" data-animate-duration="1s"><?= $card['info'] ?></p>
</div>