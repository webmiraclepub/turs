<?php

add_action('wp_ajax_miracle_blog_page', 'miracle_blog_page');
add_action('wp_ajax_nopriv_miracle_blog_page', 'miracle_blog_page');

function miracle_blog_page() {

	wp_reset_postdata();
	$nonce = $_POST['nonce'];

	if( ! wp_verify_nonce( $nonce, 'KonservaTravel' ) )
		die('Ошибка доступа');
	
	$offset = $_POST['page'] * 6 - 6;
	$args = array(
			'post_type' => 'post',
			'orderby' => 'date',
			'order' => 'ASC',
			'posts_per_page' => '1',
			'offset' => $offset
		);
	$query = new WP_Query($args);
	$posts = $query->posts;
	$html = '';

	$iterator = 0;
	foreach ( $posts as $post ):
		$title   = $post->post_title;
		$content = $post->post_excerpt;
		$url     = get_permalink( $post->ID );
		$img     = get_the_post_thumbnail_url( $post->ID, 'full' );
		$animation = ( $iterator%2 != 0 ) ? 'bounceInRight' : 'bounceInLeft';
		$post = file_get_contents( get_template_directory() . '/views_support/blog/post.php' );
		$post = str_replace('<?= $title ?>', $title, $post);
		$post = str_replace('<?= $content ?>', $content, $post);
		$post = str_replace('<?= $url ?>', $url, $post);
		$post = str_replace('<?= $animation ?>', $animation, $post);
		$post = str_replace('data-src', 'style', $post);
		$post = str_replace('<?= $img ?>', 'background-image: url('.$img.')', $post);
		$html .= $post;
		$iterator++;
	endforeach;

	$answer = array();
	$answer['posts'] = $html;
	$answer['pages'] = miracle_get_blog_pagenation_string( $_POST['page'] );
	echo json_encode($answer);
	wp_die();
}