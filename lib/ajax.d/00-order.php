<?php

add_action('wp_ajax_miracle_get_count_tours', 'order_processing');
add_action('wp_ajax_nopriv_miracle_get_count_tours', 'order_processing');

function order_processing() {

	$nonce = $_POST['nonce'];

	if( ! wp_verify_nonce( $nonce, 'KonservaTravel' ) )
		die('Ошибка доступа');
		
	$term = get_term_by( 'slug', $_POST['slug'], 'tour_cat', 'ARRAY_A' );
	echo $term['count'];
	wp_die();
}