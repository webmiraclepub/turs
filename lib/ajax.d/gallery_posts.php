<?php

add_action('wp_ajax_miracle_get_new_gallery_posts', 'miracle_get_new_gallery_posts');
add_action('wp_ajax_nopriv_miracle_get_new_gallery_posts', 'miracle_get_new_gallery_posts');

function miracle_get_new_gallery_posts() {

	$nonce = $_POST['nonce'];
	
	if( ! wp_verify_nonce( $nonce, 'KonservaTravel' ) )
		die('Ошибка доступа');
	
	$galleries = get_field( 'miracle_gallery_active_post', $_POST['id'] );
	$html = '';
	$iterator = 1;
	$count = $_POST['count'];
	$max = $count + 3 + 1;
	foreach( $galleries as $gallery ):
		if( $iterator > $count && $iterator < $max ):
			$download    = get_template_directory_uri() . '/zip/archive_' . $gallery->ID . '.zip';
			$index_image = get_the_post_thumbnail_url( $gallery->ID, 'full' );
			$title       = $gallery->post_title;
			$img         = get_field( 'mitacle_tour_gallery_post', $gallery->ID );
			$count       = count( $img );
			$year        = get_field( 'mitacle_tour_gallery_post_year', $gallery->ID );
			$num = $iterator % 2;
			if( $num == 0 ):
				$gall = file_get_contents( get_template_directory() . '/views_support/gallery_card_even.php' );
			else:
				$gall = file_get_contents( get_template_directory() . '/views_support/gallery_card_odd.php' );
			endif;
			$gall = str_replace('<?= $download ?>', $download, $gall);
			$gall = str_replace('<?= $index_image ?>', $index_image, $gall);
			$gall = str_replace('<?= $title ?>', $title, $gall);
			$gall = str_replace('<?= $img[0][\'url\'] ?>', $img[0]['url'], $gall);
			$gall = str_replace('<?= $img[1][\'url\'] ?>', $img[1]['url'], $gall);
			$gall = str_replace('<?= $img[2][\'url\'] ?>', $img[2]['url'], $gall);
			$gall = str_replace('<?= $img[3][\'url\'] ?>', $img[3]['url'], $gall);
			$gall = str_replace('<?= $img[4][\'url\'] ?>', $img[4]['url'], $gall);
			$gall = str_replace('<?= $img[5][\'url\'] ?>', $img[5]['url'], $gall);
			$gall = str_replace('<?= $img[6][\'url\'] ?>', $img[6]['url'], $gall);
			$gall = str_replace('<?= $img[0][\'alt\'] ?>', $img[0]['alt'], $gall);
			$gall = str_replace('<?= $img[1][\'alt\'] ?>', $img[1]['alt'], $gall);
			$gall = str_replace('<?= $img[2][\'alt\'] ?>', $img[2]['alt'], $gall);
			$gall = str_replace('<?= $img[3][\'alt\'] ?>', $img[3]['alt'], $gall);
			$gall = str_replace('<?= $img[4][\'alt\'] ?>', $img[4]['alt'], $gall);
			$gall = str_replace('<?= $img[5][\'alt\'] ?>', $img[5]['alt'], $gall);
			$gall = str_replace('<?= $img[6][\'alt\'] ?>', $img[6]['alt'], $gall);
			$gall = str_replace('<?= $img[0][\'title\'] ?>', $img[0]['title'], $gall);
			$gall = str_replace('<?= $img[1][\'title\'] ?>', $img[1]['title'], $gall);
			$gall = str_replace('<?= $img[2][\'title\'] ?>', $img[2]['title'], $gall);
			$gall = str_replace('<?= $img[3][\'title\'] ?>', $img[3]['title'], $gall);
			$gall = str_replace('<?= $img[4][\'title\'] ?>', $img[4]['title'], $gall);
			$gall = str_replace('<?= $img[5][\'title\'] ?>', $img[5]['title'], $gall);
			$gall = str_replace('<?= $img[6][\'title\'] ?>', $img[6]['title'], $gall);
			$gall = str_replace('data-src', 'src', $gall);
			$gall = str_replace('<?= $count ?>', $count, $gall);
			$gall = str_replace('<?= $year ?>', $year, $gall);
			$html .= $gall;
		endif;
		$iterator++;
	endforeach;
	echo $html;
	wp_die();
}