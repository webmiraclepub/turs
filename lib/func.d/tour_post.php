<?php

/**
 *   The register post-type tour
 */

add_action('init', 'mis_tour_post_init');
function mis_tour_post_init(){
	register_post_type('tour', array(
		'labels'             => array(
			'name'               => 'Туры',
			'singular_name'      => 'Туры',
			'add_new'            => 'Новый тур',
			'add_new_item'       => 'Добавить новую тур',
			'edit_item'          => 'Редактировать тур',
			'new_item'           => 'Новый тур',
			'view_item'          => 'Посмотреть тур',
			'search_items'       => 'Найти тур',
			'not_found'          => 'В организации не найдено тура',
			'not_found_in_trash' => 'В корзине не найдено тура',
			'parent_item_colon'  => '',
			'menu_name'          => 'Туры'
		  ),
                'description'        => 'Здесь хранятся все галерии путешествий',
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => true,
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => 20,
		'menu_icon'          => 'dashicons-editor-paste-text',
		'supports'           => array
					(
					'title',
					)
	) );
}