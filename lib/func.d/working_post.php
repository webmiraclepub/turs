<?php

/**
 *   The register post-type working
 */

add_action('init', 'mis_working_post_init');
function mis_working_post_init(){
	register_post_type('working', array(
		'labels'             => array(
			'name'               => 'Команда сотрудников',
			'singular_name'      => 'Сотрудник',
			'add_new'            => 'Новый сотрудник',
			'add_new_item'       => 'Добавить нового сотрудника',
			'edit_item'          => 'Редактировать данные сотрудника',
			'new_item'           => 'Новый сотрудник',
			'view_item'          => 'Посмотреть сотрудников',
			'search_items'       => 'Найти сотрудника',
			'not_found'          => 'В организации не найден сотрудник',
			'not_found_in_trash' => 'В корзине не найден сотрудник',
			'parent_item_colon'  => '',
			'menu_name'          => 'Команда сотрудников',
                        'featured_image'     => 'Фото сотрудника',
                        'set_featured_image' => 'Установить фото',
                        'remove_featured_image' => 'Удалить фото',
                        'use_featured_image' => 'Использовать как фото сотрудника',

		  ),
                'description'        => 'Здесь хранятся все сотрудники организации',
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => true,
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => 20,
                'menu_icon'          => 'dashicons-groups',
		'supports'           => array
					(
 					'title',
					'thumbnail',
					)
	) );
}

add_theme_support( 'post-thumbnails', array( 'working' ) );