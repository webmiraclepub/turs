<?php

/**
 *   The register post-type question
 */

add_action('init', 'mis_question_post_init');
function mis_question_post_init(){
	register_post_type('question', array(
		'labels'             => array(
			'name'               => 'FAQ',
			'singular_name'      => 'Вопрос',
			'add_new'            => 'Новый вопрос',
			'add_new_item'       => 'Добавить новый вопрос',
			'edit_item'          => 'Редактировать вопрос',
			'new_item'           => 'Новый вопрос',
			'view_item'          => 'Посмотреть вопрос',
			'search_items'       => 'Найти вопрос',
			'not_found'          => 'Вопрос не найдено',
			'not_found_in_trash' => 'В корзине не найдено вопрос',
			'parent_item_colon'  => '',
			'menu_name'          => 'FAQ'
		  ),
                'description'        => 'Здесь хранятся все ответы на часто задаваемые вопросы',
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => true,
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => 20,
		'menu_icon'          => 'dashicons-editor-help',
		'supports'           => array
					(
					'title'
					)
	) );
}