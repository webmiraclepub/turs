<?php

// Register Tour Taxonomy

function custom_taxonomy_tour() {

	$labels = array(
		'name'                       => 'Сезон',
		'singular_name'              => 'Сезон тура',
		'menu_name'                  => 'Сезоны',
		'all_items'                  => 'Все сезоны',
		'parent_item'                => 'Родительский сезон',
		'parent_item_colon'          => 'Родительский сезон:',
		'new_item_name'              => 'Новый сезон',
		'add_new_item'               => 'Добавить сезон',
		'edit_item'                  => 'Редактировать сезон',
		'update_item'                => 'Обновить',
		'view_item'                  => 'Вид',
		'separate_items_with_commas' => 'Отделить элементы запятыми',
		'add_or_remove_items'        => 'Добавить или удалить сезоны',
		'choose_from_most_used'      => __( 'Choose from most used'),
		'popular_items'              => __( 'Популярные'),
		'search_items'               => __( 'Поиск'),
		'not_found'                  => __( 'Не найдено'),
	);
	$rewrite = array(
		'slug'                       => 'tour_cat',
		'with_front'                 => true,
		'hierarchical'               => false,
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => false,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'rewrite'                    => $rewrite,
	);
	register_taxonomy( 'tour_cat', array( 'tour' ), $args );

}
add_action( 'init', 'custom_taxonomy_tour', 0 );