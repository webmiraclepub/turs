<?php

/**
 *   The register post-type video
 */

add_action('init', 'mis_video_post_init');
function mis_video_post_init(){
	register_post_type('video', array(
		'labels'             => array(
			'name'               => 'Видеоотчеты',
			'singular_name'      => 'Видеоотчет',
			'add_new'            => 'Новый видеоотчет',
			'add_new_item'       => 'Добавить новый видеоотчет',
			'edit_item'          => 'Редактировать видеоотчет',
			'new_item'           => 'Новый видеоотчет',
			'view_item'          => 'Посмотреть видеоотчет',
			'search_items'       => 'Найти видеоотчет',
			'not_found'          => 'В организации не найдено видеоотчета',
			'not_found_in_trash' => 'В корзине не найдено видеоотчета',
			'parent_item_colon'  => '',
			'menu_name'          => 'Видеоотчеты'
		  ),
                'description'        => 'Здесь хранятся все видеоотчеты',
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => true,
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => 20,
		'menu_icon'          => 'dashicons-video-alt3',
		'supports'           => array
					(
					'title',
					)
	) );
}