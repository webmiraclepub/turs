<?php

/**
 *   The register post-type tour-gallery
 */

add_action('init', 'mis_tour_gallery_post_init');
function mis_tour_gallery_post_init(){
	register_post_type('tour-gallery', array(
		'labels'             => array(
			'name'               => 'Галерея путешествий',
			'singular_name'      => 'Галерея',
			'add_new'            => 'Новая галерея',
			'add_new_item'       => 'Добавить новую галерею',
			'edit_item'          => 'Редактировать галерею',
			'new_item'           => 'Новая галерея',
			'view_item'          => 'Посмотреть галерею',
			'search_items'       => 'Найти галерею',
			'not_found'          => 'В организации не найдено галереи',
			'not_found_in_trash' => 'В корзине не найдено галереи',
			'parent_item_colon'  => '',
			'menu_name'          => 'Галерея путешествий',
                        'featured_image'     => 'Главное изображение',
                        'set_featured_image' => 'Установить фото',
                        'remove_featured_image' => 'Удалить фото',
                        'use_featured_image' => 'Использовать как главное фото',

		  ),
                'description'        => 'Здесь хранятся все галерии путешествий',
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => true,
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => 20,
		'menu_icon'          => 'dashicons-images-alt2',
		'supports'           => array
					(
					'title',
					'thumbnail',
					)
	) );
}

add_theme_support( 'post-thumbnails', array( 'tour-gallery' ) );