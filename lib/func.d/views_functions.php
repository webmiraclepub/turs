<<<<<<< HEAD
<?php

function miracle_get_menu_item( $class = '' ){
	
	if( is_page_template( 'pagetemplates/main.php' ) ):
	?>
	
	<ul class="<?= $class ?> menu" data-magellan>

	<?php
		$menus = get_field('home_menu');
		foreach ( $menus as $value):
    	    $url = ( $value['url'] == 'blog' ) ? get_post_type_archive_link('post') : $value['url'];
    ?>

    <li class="text-center"><a href="<?= $url ?>"><?= $value['menu_item'] ?></a></li>
    
    <?php
    	endforeach;
    ?>

    </ul>

    <?php
    
    else:
    	if( $class == 'vertical' ):
			wp_nav_menu( array(
				'theme_location'  => 'main',
				'menu'            => '', 
				'container'       => '', 
				'container_class' => '',
				'container_id'    => '',
				'menu_class'      => 'menu vertical',
				'menu_id'         => '',
				'echo'            => true,
				'fallback_cb'     => '__return_empty_string',
				'before'          => '',
				'after'           => '',
				'link_before'     => '',
				'link_after'      => '',
				'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
				'depth'           => 0,
			) );
		else:
			wp_nav_menu( array(
				'theme_location'  => 'main',
				'menu'            => '', 
				'container'       => 'div', 
				'container_class' => 'cell menu-centered',
				'container_id'    => '',
				'menu_class'      => 'menu expanded',
				'menu_id'         => '',
				'echo'            => true,
				'fallback_cb'     => '__return_empty_string',
				'before'          => '',
				'after'           => '',
				'link_before'     => '',
				'link_after'      => '',
				'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
				'depth'           => 0,
			) );
		endif;
	endif;
}

function miracle_get_sidebar(){
	if( is_page_template( 'pagetemplates/main.php' ) ):
	  get_sidebar( 'home' );
	else:
	  get_sidebar();
	endif;
}

function miracle_get_about_us_card(){
	$cards = get_field('miracle_about_info');
	foreach ($cards as $card):
		include( get_template_directory() . '/views_support/about_us_card.php' );
	endforeach;
	?>
	<div class="cell info-card">
		<p data-animate="tada" data-animate-delay="3s"><img src="<?= get_template_directory_uri() ?>/dist/assets/images/besk.svg"></p>
		<p data-animate="fadeInDown" data-animate-delay="0s" data-animate-duration="1s">Незабываемых эмоций</p>
	</div>
	<?php
}

function miracle_get_working_card(){
	$work_posts = get_field( 'miracle_working_carousel' );
	foreach ($work_posts as $working):
		$id = $working->ID;
		$image = get_the_post_thumbnail_url( $working->ID, 'full' );
		$title = $working->post_title;
		$status = get_field( 'miracle_post_working_status', $id );
		$icons = get_field( 'miracle_post_working_social', $working->ID );
		include( get_template_directory() . '/views_support/working_card.php' );
	endforeach;
}

function miracle_get_working_social_icons( $icons ){
	foreach ( $icons as $icon):
		$url = $icon['url'];
		$img = '';
		if( $icon['icon'] == 'F' ):
			$img = 'facebook';
		elseif( $icon['icon'] == 'Ins' ):
			$img = 'instagram';
		elseif( $icon['icon'] == 'VK' ):
			$img = 'vk';
		endif;
	?>

		<li><a href="<?= $url ?>"><i class="miracle-icon-<?= $img ?>"></i></a></li>
	
	<?php
	endforeach;
}

function miracle_get_gallery_card( $i = 1 ){
	$gallerys = get_field('miracle_gallery_active_post');
	foreach( $gallerys as $gallery ):
		if( $i > 0 ):
			$download    = get_template_directory_uri() . '/zip/archive_' . $gallery->ID . '.zip';
			$index_image = get_the_post_thumbnail_url( $gallery->ID, 'full' );
			$title       = $gallery->post_title;
			$img         = get_field( 'mitacle_tour_gallery_post', $gallery->ID );
			$count       = count( $img );
			$year        = get_field( 'mitacle_tour_gallery_post_year', $gallery->ID );
			$num = $i % 2;
			if( $num == 0 ):
				include( get_template_directory() . '/views_support/gallery_card_even.php' );
			else:
				include( get_template_directory() . '/views_support/gallery_card_odd.php' );
			endif;
		endif;
		$i--;
	endforeach;
}

function miracle_get_reveiw_item(){
	$reveiws = get_field( 'miracle_videootchet_active_post' );
	foreach ( $reveiws as $reveiw ):
		$title = $reveiw->post_title;
		$url = get_field( 'miracle_post_video_url', $reveiw->ID );
		$year = get_field( 'miracle_video_post_year', $reveiw->ID );
		include( get_template_directory() . '/views_support/reveiw_card.php' );
	endforeach;
}

function miracle_get_comment_card(){
	$comments = get_field( 'miracle_review_active_post' );
	foreach ($comments as $comment):
		$title   = $comment->post_title;
		$img     = get_the_post_thumbnail_url( $comment->ID, 'full' );
		$content = strip_tags($comment->post_content);
		$status  = get_field( 'miracle_comment_post_status', $comment->ID );
		include( get_template_directory() . '/views_support/comment_card.php' );
	endforeach;
}

function miracle_get_question(){
	$questions = get_field( 'miracle_question_active_post' );
	foreach ( $questions as $question ):
		$quest = get_field( 'miracle_question_post_title', $question->ID );
		$answer = get_field( 'miracle_question_post_answer', $question->ID );
		include( get_template_directory() . '/views_support/question_card.php' );
	endforeach;
}

function miracle_get_active_tours(){
	$all_tours = get_field( 'miracle_tours_active_posts' );
	$winter_tours = array();
	$spring_tours = array();
	$summer_tours = array();
	$autumn_tours = array();
	foreach ($all_tours as $tour):
		$taxonomy = get_field( 'miracle_tour_post_taxonomy', $tour->ID );
		if( $taxonomy->slug == 'winter' ):
			array_push( $winter_tours, $tour );
		elseif( $taxonomy->slug == 'spring' ):
			array_push( $spring_tours, $tour );
		elseif( $taxonomy->slug == 'autumn' ):
			array_push( $autumn_tours, $tour );
		elseif( $taxonomy->slug == 'summer' ):
			array_push( $summer_tours, $tour );
		endif;
	endforeach;
	include( get_template_directory() . '/views_support/tours/tours_winter.php' );
	include( get_template_directory() . '/views_support/tours/tours_spring.php' );
	include( get_template_directory() . '/views_support/tours/tours_summer.php' );
	include( get_template_directory() . '/views_support/tours/tours_autumn.php' );
}

function miracle_add_tour_info( $s_tours ){
	global $active_tour_ids;
	foreach ( $s_tours as $tour_post ):
		$id        = $tour_post->ID;
		$active_tour_ids[] = $id;
		$title     = $tour_post->post_title;
		$toggler   = "toggler-" . $tour_post->ID;
		$data_on   = get_field( 'mitacle_tour_data_on', $tour_post->ID );
		$data_off  = get_field( 'mitacle_tour_data_off', $tour_post->ID );
		$count     = get_field( 'mitacle_tour_count', $tour_post->ID );
		$fishka    = get_field( 'mitacle_tour_fishka', $tour_post->ID );
		$old_price = get_field( 'mitacle_tour_old_price', $tour_post->ID );
		$new_price = get_field( 'mitacle_tour_new_price', $tour_post->ID );
		$active	   = get_field( 'горячий_тур', $tour_post->ID );
		if ( $active ){
			$active = 'active_tour';
		}
		if ( !empty( $active ) ){
			$actives[] = $tour_post;
		} else {
			include( get_template_directory() . '/views_support/tours/tour_info.php' );
		}
	endforeach;
	if ( !empty( $actives ) ){
		foreach ($actives as $active_tour_post) {
			$id        = $active_tour_post->ID;
			$active_tour_ids[] = $id;
			$title     = $active_tour_post->post_title;
			$toggler   = "toggler-" . $active_tour_post->ID;
			$data_on   = get_field( 'mitacle_tour_data_on', $active_tour_post->ID );
			$data_off  = get_field( 'mitacle_tour_data_off', $active_tour_post->ID );
			$count     = get_field( 'mitacle_tour_count', $active_tour_post->ID );
			$fishka    = get_field( 'mitacle_tour_fishka', $active_tour_post->ID );
			$old_price = get_field( 'mitacle_tour_old_price', $active_tour_post->ID );
			$new_price = get_field( 'mitacle_tour_new_price', $active_tour_post->ID );
			$active = 'active_tour';
			include( get_template_directory() . '/views_support/tours/tour_info.php' );
		}
	}
}

function miracle_mobile_tour_info( $s_tours ){
	global $program_tour_ids;
	foreach ( $s_tours as $tour_post ):
		$id        = $tour_post->ID;
		$program_tour_ids[] = $id;
		$title     = $tour_post->post_title;
		// $toggler   = "toggler-" . $tour_post->ID;
		$data_on   = get_field( 'mitacle_tour_data_on', $tour_post->ID );
		$data_off  = get_field( 'mitacle_tour_data_off', $tour_post->ID );
		// $count     = get_field( 'mitacle_tour_count', $tour_post->ID );
		// $fishka    = get_field( 'mitacle_tour_fishka', $tour_post->ID );
		// $old_price = get_field( 'mitacle_tour_old_price', $tour_post->ID );
		// $new_price = get_field( 'mitacle_tour_new_price', $tour_post->ID );
		// $active	   = get_field( 'горячий_тур', $tour_post->ID );
		if ( $active ){
			$active = 'active_tour';
		}
		include( get_template_directory() . '/views_support/tours/tour_mobile_info.php' );
	endforeach;
}

function miracle_get_tour_slider( $id ){
	$program = get_field( 'miracle_tour_program', $id );
	if ( !empty( $program ) ):
		foreach ($program as $day_program):
			$img     = $day_program['image'];
			$url     = $img['url'];
			$alt     = $img['alt'];
			$title   = $img['caption'];
			$day     = $day_program['day'];
			$content = str_replace('"', '\'\'', $day_program['content']);
			include( get_template_directory() . '/views_support/tours/tour_slide.php' );
		endforeach;
	endif;
}

function miracle_get_tour_slide_info( $id ){
	$program = get_field( 'miracle_tour_program', $id );
	$iterator = 0;
	if ( !empty( $program ) ):
		foreach ($program as $day_program):
			if( $iterator == 0 ):
				$day     = $day_program['day'];
				$content = $day_program['content'];
				include( get_template_directory() . '/views_support/tours/tour_slide_info.php' );
			else:
				break;
			endif;
			$iterator++;
		endforeach;
	endif;
}

function miracle_get_tour_includes( $id ){
	$includes = get_field( 'miracle_tour_includes', $id );
	if ( !empty( $includes ) ):
		foreach ($includes as $include):
			$class = ( $include['icommon'] ) ? $include['icon-i'] : $include['icon'];
		?>
			<div class="cell">
				<i class="fa <?= $class ?>"></i><br>
				<?= $include['name'] ?>
			</div>
		<?php
		endforeach;
	endif;
}

function miracle_add_tour_gallery_carousel( $id ){
	$gallery = get_field( 'miracle_tour_gallery_images', $id );
	$index_image = get_the_post_thumbnail_url( $gallery->ID, 'full' );
	$title = $gallery->post_title;
	$images = get_field( 'mitacle_tour_gallery_post', $gallery->ID );
	$year = get_field( 'mitacle_tour_gallery_post_year', $gallery->ID );
	$img_count = count( $images );
	$iterator = ( $img_count % 8 );
	$iterator2 = 0;
	do {
		$img[0] = $images[0+7*$iterator2];
		$img[1] = $images[1+7*$iterator2];
		$img[2] = $images[2+7*$iterator2];
		$img[3] = $images[3+7*$iterator2];
		$img[4] = $images[4+7*$iterator2];
		$img[5] = $images[5+7*$iterator2];
		$img[6] = $images[6+7*$iterator2];
		include( get_template_directory() . '/views_support/tours/tour_gallery.php' );
		$iterator--;
		$iterator2++;
	} while( isset( $images[6+7*$iterator2] ) );
}

function miracle_get_form( $show_tour = false, $tour = '' ){
	// echo do_shortcode('[contact-form-7 id="282" title="Сбор данных"]');
	?>
	<form method="POST" data-abide novalidate>
		<input type="text" name="name" placeholder="ФИО">
		<input type="email" name="email" placeholder="E-MAIL">
		<input type="text" required name="phone" placeholder="+38 (__) ___ __ __">
	<?php
		if( $show_tour ):
	?>
		<input type="hidden" name="tour" value="<?= $tour ?>">
	<?php
		endif;
	?>
		<span class="info hidden label"></span>
		<input type="submit" class="button orange" value="бронировать">
	</form>
	<?php
}

function miracle_get_social_site(){
	$sites = get_field('miracle-global-social-site', 'option');
	foreach ($sites as $site):
		$url = $site['url'];
		$title = $site['name'];
		$icon = $site['icon'];
?>
	<li>
		<a href="<?= $url ?>">
			<i class="fa <?= $icon ?>" title="<?= $title ?>"></i>
		</a>
	</li>
<?php
	endforeach;
}

function miracle_get_before_footer(){
	if( is_page_template( 'pagetemplates/main.php' ) ):
		get_template_part('pagetemplates/main_sections/question');
	else:
		$img_left  = get_template_directory_uri() . '/dist/assets/images/stars.png';
		$img_right = get_template_directory_uri() . '/dist/assets/images/box.svg';
		include( get_template_directory() . '/views_support/before_footer.php' );
	endif;
}

function miracle_get_all_posts(){
	include( get_template_directory() . '/views_support/blog/all_posts.php' );
}

function miracle_get_post_card( $offset = 0 ){
	if( is_search() ):
		$posts = query_posts( 's='.get_search_query() );
	else:
		$page=(get_query_var('paged')) ? get_query_var('paged') : 1;
		$args = array(
			'post_type' => 'post',
			'orderby' => 'date',
			'order' => 'ASC',
			'posts_per_page' => '6',
			'offset' => $offset,
			'paged' => $page
			);
		$query = new WP_Query($args);
		$posts = $query->posts;
	endif;
	$iterator = 0;
	foreach ( $posts as $post ):
		$title   = $post->post_title;
		$content = $post->post_excerpt;
		$url     = get_permalink( $post->ID );
		$img     = get_the_post_thumbnail_url( $post->ID, 'full' );
		$animation = ( $iterator%2 != 0 ) ? 'bounceInRight' : 'bounceInLeft';
		include( get_template_directory() . '/views_support/blog/post.php' );
		$iterator++;
	endforeach;
}

function miracle_get_blog_pagenation(){
	include( get_template_directory() . '/views_support/blog/pagenation.php' );
}

function miracle_get_pagenation_numeric( $page = 1 ){
	$count_posts = wp_count_posts( 'post' )->publish;
	$ost = $count_posts%6;
	$pages = ( $count_posts - $ost ) / 6 + 0;

	if( $pages > 7 ):

		$last = $pages - 4;

		if( $page < 4 ){
			include( get_template_directory() . '/views_support/blog/page_numeric/arrow/prev/deactive.php' );
		}else{
			$i = $page - 3;
			include( get_template_directory() . '/views_support/blog/page_numeric/arrow/prev/active.php' );
		}

		if( $page < 5 ):
			for($i = 1; $i <= $pages; $i++):
				if( ( $i < 6 && $i != $page ) || $i == $pages ):
					include( get_template_directory() . '/views_support/blog/page_numeric/visible_page.php' );
				elseif( $i == $page ):
				 	include( get_template_directory() . '/views_support/blog/page_numeric/active_page.php' );
				elseif( $i == ( $pages - 1 ) ):
				 	include( get_template_directory() . '/views_support/blog/page_numeric/after_first_page.php' );
				else:
				 	include( get_template_directory() . '/views_support/blog/page_numeric/hide_page.php' );
				endif;
			endfor;
		elseif( $page < $last ):
			for($i = 1; $i <= $pages; $i++):
				if( $i == 1 || $i == $pages || ( $i == ( $page - 1 ) ) || ( $i == ( $page + 1 ) ) ):
					include( get_template_directory() . '/views_support/blog/page_numeric/visible_page.php' );
				elseif( $i == 2 || $i == ( $pages - 1 ) ):
					include( get_template_directory() . '/views_support/blog/page_numeric/after_first_page.php' );
				elseif( $i == $page ):
					include( get_template_directory() . '/views_support/blog/page_numeric/active_page.php' );
				else:
					include( get_template_directory() . '/views_support/blog/page_numeric/hide_page.php' );
				endif;
			endfor;
		elseif( $page == $last ):
			for($i = 1; $i <= $pages; $i++):
				if( $i == 1 || $i > $last ):
					include( get_template_directory() . '/views_support/blog/page_numeric/visible_page.php' );
				elseif( $i == 2 ):
					include( get_template_directory() . '/views_support/blog/page_numeric/after_first_page.php' );
				elseif( $i == $last ):
					include( get_template_directory() . '/views_support/blog/page_numeric/active_page.php' );
				else:
					include( get_template_directory() . '/views_support/blog/page_numeric/hide_page.php' );
				endif;
			endfor;
		elseif( $page > $last ):
			for($i = 1; $i <= $pages; $i++):
				if( $i == 1 || ( $i >= $last && $i != $page ) ):
					include( get_template_directory() . '/views_support/blog/page_numeric/visible_page.php' );
				elseif( $i == 2 ):
					include( get_template_directory() . '/views_support/blog/page_numeric/after_first_page.php' );
				elseif( $i == $page ):
					include( get_template_directory() . '/views_support/blog/page_numeric/active_page.php' );
				else:
					include( get_template_directory() . '/views_support/blog/page_numeric/hide_page.php' );
				endif;
			endfor;
		endif;


		if( $page > ($pages - 4) ){
			include( get_template_directory() . '/views_support/blog/page_numeric/arrow/next/deactive.php' );
		}else{
			$i = $page + 3;
			include( get_template_directory() . '/views_support/blog/page_numeric/arrow/next/active.php' );
		}
	elseif( $pages == 1 ):
	else:
		for($i = 1; $i <= $pages; $i++):
			if( $i == $page ):
				include( get_template_directory() . '/views_support/blog/page_numeric/active_page.php' );
			else:
				include( get_template_directory() . '/views_support/blog/page_numeric/visible_page.php' );
			endif;
		endfor;
	endif;
}

function miracle_get_blog_pagenation_string( $page = 1 ){
	$html = '';
	$count_posts = wp_count_posts( 'post' )->publish;
	$ost = $count_posts%1;
	$pages = ( $count_posts - $ost ) / 1 + 0;

	if( $pages > 7 ):

		$last = $pages - 4;

		if( $page < 4 ){
			$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/arrow/prev/deactive.php' );
			$html = str_replace('<?= $i ?>', $i, $html);
		}else{
			$i = $page - 3;
			$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/arrow/prev/active.php' );
			$html = str_replace('<?= $i ?>', $i, $html);
		}

		if( $page < 5 ):
			for($i = 1; $i <= $pages; $i++):
				if( ( $i < 6 && $i != $page ) || $i == $pages ):
					$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/visible_page.php' );
					$html = str_replace('<?= $i ?>', $i, $html);
				elseif( $i == $page ):
				 	$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/active_page.php' );
					$html = str_replace('<?= $i ?>', $i, $html);
				elseif( $i == ( $pages - 1 ) ):
				 	$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/after_first_page.php' );
					$html = str_replace('<?= $i ?>', $i, $html);
				else:
				 	$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/hide_page.php' );
					$html = str_replace('<?= $i ?>', $i, $html);
				endif;
			endfor;
		elseif( $page < $last ):
			for($i = 1; $i <= $pages; $i++):
				if( $i == 1 || $i == $pages || ( $i == ( $page - 1 ) ) || ( $i == ( $page + 1 ) ) ):
					$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/visible_page.php' );
					$html = str_replace('<?= $i ?>', $i, $html);
				elseif( $i == 2 || $i == ( $pages - 1 ) ):
					$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/after_first_page.php' );
					$html = str_replace('<?= $i ?>', $i, $html);
				elseif( $i == $page ):
					$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/active_page.php' );
					$html = str_replace('<?= $i ?>', $i, $html);
				else:
					$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/hide_page.php' );
					$html = str_replace('<?= $i ?>', $i, $html);
				endif;
			endfor;
		elseif( $page == $last ):
			for($i = 1; $i <= $pages; $i++):
				if( $i == 1 || $i > $last ):
					$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/visible_page.php' );
					$html = str_replace('<?= $i ?>', $i, $html);
				elseif( $i == 2 ):
					$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/after_first_page.php' );
					$html = str_replace('<?= $i ?>', $i, $html);
				elseif( $i == $last ):
					$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/active_page.php' );
					$html = str_replace('<?= $i ?>', $i, $html);
				else:
					$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/hide_page.php' );
					$html = str_replace('<?= $i ?>', $i, $html);
				endif;
			endfor;
		elseif( $page > $last ):
			for($i = 1; $i <= $pages; $i++):
				if( $i == 1 || ( $i >= $last && $i != $page ) ):
					$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/visible_page.php' );
					$html = str_replace('<?= $i ?>', $i, $html);
				elseif( $i == 2 ):
					$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/after_first_page.php' );
					$html = str_replace('<?= $i ?>', $i, $html);
				elseif( $i == $page ):
					$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/active_page.php' );
					$html = str_replace('<?= $i ?>', $i, $html);
				else:
					$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/hide_page.php' );
					$html = str_replace('<?= $i ?>', $i, $html);
				endif;
			endfor;
		endif;


		if( $page > ($pages - 4) ){
			$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/arrow/next/deactive.php' );
			$html = str_replace('<?= $i ?>', $i, $html);
		}else{
			$i = $page + 3;
			$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/arrow/next/active.php' );
			$html = str_replace('<?= $i ?>', $i, $html);
		}
	elseif( $pages == 1 ):
	else:
		for($i = 1; $i <= $pages; $i++):
			if( $i == $page ):
				$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/active_page.php' );
				$html = str_replace('<?= $i ?>', $i, $html);
			else:
				$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/visible_page.php' );
				$html = str_replace('<?= $i ?>', $i, $html);
			endif;
		endfor;
	endif;

	$html = str_replace('<?= $i ?>', $i, $html);
	return $html;
=======
<?php

function miracle_get_menu_item( $class = '' ){
	
	if( is_page_template( 'pagetemplates/main.php' ) ):
	?>
	
	<ul class="<?= $class ?> menu" data-magellan>

	<?php
		$menus = get_field('home_menu');
		foreach ( $menus as $value):
    	    $url = ( $value['url'] == 'blog' ) ? get_post_type_archive_link('post') : $value['url'];
    ?>

    <li class="text-center"><a href="<?= $url ?>"><?= $value['menu_item'] ?></a></li>
    
    <?php
    	endforeach;
    ?>

    </ul>

    <?php
    
    else:
    	if( $class == 'vertical' ):
			wp_nav_menu( array(
				'theme_location'  => 'main',
				'menu'            => '', 
				'container'       => '', 
				'container_class' => '',
				'container_id'    => '',
				'menu_class'      => 'menu vertical',
				'menu_id'         => '',
				'echo'            => true,
				'fallback_cb'     => '__return_empty_string',
				'before'          => '',
				'after'           => '',
				'link_before'     => '',
				'link_after'      => '',
				'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
				'depth'           => 0,
			) );
		else:
			wp_nav_menu( array(
				'theme_location'  => 'main',
				'menu'            => '', 
				'container'       => 'div', 
				'container_class' => 'cell menu-centered',
				'container_id'    => '',
				'menu_class'      => 'menu expanded',
				'menu_id'         => '',
				'echo'            => true,
				'fallback_cb'     => '__return_empty_string',
				'before'          => '',
				'after'           => '',
				'link_before'     => '',
				'link_after'      => '',
				'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
				'depth'           => 0,
			) );
		endif;
	endif;
}

function miracle_get_sidebar(){
	if( is_page_template( 'pagetemplates/main.php' ) ):
	  get_sidebar( 'home' );
	else:
	  get_sidebar();
	endif;
}

function miracle_get_about_us_card(){
	$cards = get_field('miracle_about_info');
	foreach ($cards as $card):
		include( get_template_directory() . '/views_support/about_us_card.php' );
	endforeach;
	?>
	<div class="cell info-card">
		<p data-animate="tada" data-animate-delay="3s"><img src="<?= get_template_directory_uri() ?>/dist/assets/images/besk.svg"></p>
		<p data-animate="fadeInDown" data-animate-delay="0s" data-animate-duration="1s">Незабываемых эмоций</p>
	</div>
	<?php
}

function miracle_get_working_card(){
	$work_posts = get_field( 'miracle_working_carousel' );
	foreach ($work_posts as $working):
		$id = $working->ID;
		$image = get_the_post_thumbnail_url( $working->ID, 'full' );
		$title = $working->post_title;
		$status = get_field( 'miracle_post_working_status', $id );
		$icons = get_field( 'miracle_post_working_social', $working->ID );
		include( get_template_directory() . '/views_support/working_card.php' );
	endforeach;
}

function miracle_get_working_social_icons( $icons ){
	foreach ( $icons as $icon):
		$url = $icon['url'];
		$img = '';
		if( $icon['icon'] == 'F' ):
			$img = 'facebook';
		elseif( $icon['icon'] == 'Ins' ):
			$img = 'instagram';
		elseif( $icon['icon'] == 'VK' ):
			$img = 'vk';
		endif;
	?>

		<li><a href="<?= $url ?>"><i class="miracle-icon-<?= $img ?>"></i></a></li>
	
	<?php
	endforeach;
}

function miracle_get_gallery_card( $i = 1 ){
	$gallerys = get_field('miracle_gallery_active_post');
	foreach( $gallerys as $gallery ):
		if( $i > 0 ):
			$download    = get_template_directory_uri() . '/zip/archive_' . $gallery->ID . '.zip';
			$index_image = get_the_post_thumbnail_url( $gallery->ID, 'full' );
			$title       = $gallery->post_title;
			$img         = get_field( 'mitacle_tour_gallery_post', $gallery->ID );
			$count       = count( $img );
			$year        = get_field( 'mitacle_tour_gallery_post_year', $gallery->ID );
			$num = $i % 2;
			if( $num == 0 ):
				include( get_template_directory() . '/views_support/gallery_card_even.php' );
			else:
				include( get_template_directory() . '/views_support/gallery_card_odd.php' );
			endif;
		endif;
		$i--;
	endforeach;
}

function miracle_get_reveiw_item(){
	$reveiws = get_field( 'miracle_videootchet_active_post' );
	foreach ( $reveiws as $reveiw ):
		$title = $reveiw->post_title;
		$url = get_field( 'miracle_post_video_url', $reveiw->ID );
		$year = get_field( 'miracle_video_post_year', $reveiw->ID );
		include( get_template_directory() . '/views_support/reveiw_card.php' );
	endforeach;
}

function miracle_get_comment_card(){
	$comments = get_field( 'miracle_review_active_post' );
	foreach ($comments as $comment):
		$title   = $comment->post_title;
		$img     = get_the_post_thumbnail_url( $comment->ID, 'full' );
		$content = $comment->post_content;
		$status  = get_field( 'miracle_comment_post_status', $comment->ID );
		include( get_template_directory() . '/views_support/comment_card.php' );
	endforeach;
}

function miracle_get_question(){
	$questions = get_field( 'miracle_question_active_post' );
	foreach ( $questions as $question ):
		$quest = get_field( 'miracle_question_post_title', $question->ID );
		$answer = get_field( 'miracle_question_post_answer', $question->ID );
		include( get_template_directory() . '/views_support/question_card.php' );
	endforeach;
}

function miracle_get_active_tours(){
	$all_tours = get_field( 'miracle_tours_active_posts' );
	$winter_tours = array();
	$spring_tours = array();
	$summer_tours = array();
	$autumn_tours = array();
	foreach ($all_tours as $tour):
		$taxonomy = get_field( 'miracle_tour_post_taxonomy', $tour->ID );
		if( $taxonomy->slug == 'winter' ):
			array_push( $winter_tours, $tour );
		elseif( $taxonomy->slug == 'spring' ):
			array_push( $spring_tours, $tour );
		elseif( $taxonomy->slug == 'autumn' ):
			array_push( $autumn_tours, $tour );
		elseif( $taxonomy->slug == 'summer' ):
			array_push( $summer_tours, $tour );
		endif;
	endforeach;
	include( get_template_directory() . '/views_support/tours/tours_winter.php' );
	include( get_template_directory() . '/views_support/tours/tours_spring.php' );
	include( get_template_directory() . '/views_support/tours/tours_summer.php' );
	include( get_template_directory() . '/views_support/tours/tours_autumn.php' );
}

function miracle_add_tour_info( $s_tours ){
	global $active_tour_ids;
	foreach ( $s_tours as $tour_post ):
		$id        = $tour_post->ID;
		$active_tour_ids[] = $id;
		$title     = $tour_post->post_title;
		$toggler   = "toggler-" . $tour_post->ID;
		$data_on   = get_field( 'mitacle_tour_data_on', $tour_post->ID );
		$data_off  = get_field( 'mitacle_tour_data_off', $tour_post->ID );
		$count     = get_field( 'mitacle_tour_count', $tour_post->ID );
		$fishka    = get_field( 'mitacle_tour_fishka', $tour_post->ID );
		$old_price = get_field( 'mitacle_tour_old_price', $tour_post->ID );
		$new_price = get_field( 'mitacle_tour_new_price', $tour_post->ID );
		$active	   = get_field( 'горячий_тур', $tour_post->ID );
		if ( $active ){
			$active = 'active_tour';
		}
		if ( !empty( $active ) ){
			$actives[] = $tour_post;
		} else {
			include( get_template_directory() . '/views_support/tours/tour_info.php' );
		}
	endforeach;
	if ( !empty( $actives ) ){
		foreach ($actives as $active_tour_post) {
			$id        = $active_tour_post->ID;
			$active_tour_ids[] = $id;
			$title     = $active_tour_post->post_title;
			$toggler   = "toggler-" . $active_tour_post->ID;
			$data_on   = get_field( 'mitacle_tour_data_on', $active_tour_post->ID );
			$data_off  = get_field( 'mitacle_tour_data_off', $active_tour_post->ID );
			$count     = get_field( 'mitacle_tour_count', $active_tour_post->ID );
			$fishka    = get_field( 'mitacle_tour_fishka', $active_tour_post->ID );
			$old_price = get_field( 'mitacle_tour_old_price', $active_tour_post->ID );
			$new_price = get_field( 'mitacle_tour_new_price', $active_tour_post->ID );
			$active = 'active_tour';
			include( get_template_directory() . '/views_support/tours/tour_info.php' );
		}
	}
}

function miracle_mobile_tour_info( $s_tours ){
	global $program_tour_ids;
	foreach ( $s_tours as $tour_post ):
		$id        = $tour_post->ID;
		$program_tour_ids[] = $id;
		$title     = $tour_post->post_title;
		// $toggler   = "toggler-" . $tour_post->ID;
		$data_on   = get_field( 'mitacle_tour_data_on', $tour_post->ID );
		$data_off  = get_field( 'mitacle_tour_data_off', $tour_post->ID );
		// $count     = get_field( 'mitacle_tour_count', $tour_post->ID );
		// $fishka    = get_field( 'mitacle_tour_fishka', $tour_post->ID );
		// $old_price = get_field( 'mitacle_tour_old_price', $tour_post->ID );
		// $new_price = get_field( 'mitacle_tour_new_price', $tour_post->ID );
		// $active	   = get_field( 'горячий_тур', $tour_post->ID );
		if ( $active ){
			$active = 'active_tour';
		}
		include( get_template_directory() . '/views_support/tours/tour_mobile_info.php' );
	endforeach;
}

function miracle_get_tour_slider( $id ){
	$program = get_field( 'miracle_tour_program', $id );
	if ( !empty( $program ) ):
		foreach ($program as $day_program):
			$img     = $day_program['image'];
			$url     = $img['url'];
			$alt     = $img['alt'];
			$title   = $img['caption'];
			$day     = $day_program['day'];
			$content = str_replace('"', '\'\'', $day_program['content']);
			include( get_template_directory() . '/views_support/tours/tour_slide.php' );
		endforeach;
	endif;
}

function miracle_get_tour_slide_info( $id ){
	$program = get_field( 'miracle_tour_program', $id );
	$iterator = 0;
	if ( !empty( $program ) ):
		foreach ($program as $day_program):
			if( $iterator == 0 ):
				$day     = $day_program['day'];
				$content = $day_program['content'];
				include( get_template_directory() . '/views_support/tours/tour_slide_info.php' );
			else:
				break;
			endif;
			$iterator++;
		endforeach;
	endif;
}

function miracle_get_tour_includes( $id ){
	$includes = get_field( 'miracle_tour_includes', $id );
	if ( !empty( $includes ) ):
		foreach ($includes as $include):
			$class = ( $include['icommon'] ) ? $include['icon-i'] : $include['icon'];
		?>
			<div class="cell">
				<i class="fa <?= $class ?>"></i><br>
				<?= $include['name'] ?>
			</div>
		<?php
		endforeach;
	endif;
}

function miracle_add_tour_gallery_carousel( $id ){
	$gallery = get_field( 'miracle_tour_gallery_images', $id );
	$index_image = get_the_post_thumbnail_url( $gallery->ID, 'full' );
	$title = $gallery->post_title;
	$images = get_field( 'mitacle_tour_gallery_post', $gallery->ID );
	$year = get_field( 'mitacle_tour_gallery_post_year', $gallery->ID );
	$img_count = count( $images );
	$iterator = ( $img_count % 8 );
	$iterator2 = 0;
	do {
		$img[0] = $images[0+7*$iterator2];
		$img[1] = $images[1+7*$iterator2];
		$img[2] = $images[2+7*$iterator2];
		$img[3] = $images[3+7*$iterator2];
		$img[4] = $images[4+7*$iterator2];
		$img[5] = $images[5+7*$iterator2];
		$img[6] = $images[6+7*$iterator2];
		include( get_template_directory() . '/views_support/tours/tour_gallery.php' );
		$iterator--;
		$iterator2++;
	} while( isset( $images[6+7*$iterator2] ) );
}

function miracle_get_form(){
	// echo do_shortcode('[contact-form-7 id="282" title="Сбор данных"]');
	?>
	<form method="POST" data-abide novalidate>
		<input type="text" name="name" placeholder="ФИО">
		<input type="email" name="email" placeholder="E-MAIL">
		<input type="text" required name="phone" placeholder="+38 (__) ___ __ __">
		<span class="info hidden label"></span>
		<input type="submit" class="button orange" value="бронировать">
	</form>
	<?php
}

function miracle_get_social_site(){
	$sites = get_field('miracle-global-social-site', 'option');
	foreach ($sites as $site):
		$url = $site['url'];
		$title = $site['name'];
		$icon = $site['icon'];
?>
	<li>
		<a href="<?= $url ?>">
			<i class="fa <?= $icon ?>" title="<?= $title ?>"></i>
		</a>
	</li>
<?php
	endforeach;
}

function miracle_get_before_footer(){
	if( is_page_template( 'pagetemplates/main.php' ) ):
		get_template_part('pagetemplates/main_sections/question');
	else:
		$img_left  = get_template_directory_uri() . '/dist/assets/images/stars.png';
		$img_right = get_template_directory_uri() . '/dist/assets/images/box.svg';
		include( get_template_directory() . '/views_support/before_footer.php' );
	endif;
}

function miracle_get_all_posts(){
	include( get_template_directory() . '/views_support/blog/all_posts.php' );
}

function miracle_get_post_card( $offset = 0 ){
	if( is_search() ):
		$posts = query_posts( 's='.get_search_query() );
	else:
		$page=(get_query_var('paged')) ? get_query_var('paged') : 1;
		$args = array(
			'post_type' => 'post',
			'orderby' => 'date',
			'order' => 'ASC',
			'posts_per_page' => '6',
			'offset' => $offset,
			'paged' => $page
			);
		$query = new WP_Query($args);
		$posts = $query->posts;
	endif;
	$iterator = 0;
	foreach ( $posts as $post ):
		$title   = $post->post_title;
		$content = $post->post_excerpt;
		$url     = get_permalink( $post->ID );
		$img     = get_the_post_thumbnail_url( $post->ID, 'full' );
		$animation = ( $iterator%2 != 0 ) ? 'bounceInRight' : 'bounceInLeft';
		include( get_template_directory() . '/views_support/blog/post.php' );
		$iterator++;
	endforeach;
}

function miracle_get_blog_pagenation(){
	include( get_template_directory() . '/views_support/blog/pagenation.php' );
}

function miracle_get_pagenation_numeric( $page = 1 ){
	$count_posts = wp_count_posts( 'post' )->publish;
	$ost = $count_posts%6;
	$pages = ( $count_posts - $ost ) / 6 + 0;

	if( $pages > 7 ):

		$last = $pages - 4;

		if( $page < 4 ){
			include( get_template_directory() . '/views_support/blog/page_numeric/arrow/prev/deactive.php' );
		}else{
			$i = $page - 3;
			include( get_template_directory() . '/views_support/blog/page_numeric/arrow/prev/active.php' );
		}

		if( $page < 5 ):
			for($i = 1; $i <= $pages; $i++):
				if( ( $i < 6 && $i != $page ) || $i == $pages ):
					include( get_template_directory() . '/views_support/blog/page_numeric/visible_page.php' );
				elseif( $i == $page ):
				 	include( get_template_directory() . '/views_support/blog/page_numeric/active_page.php' );
				elseif( $i == ( $pages - 1 ) ):
				 	include( get_template_directory() . '/views_support/blog/page_numeric/after_first_page.php' );
				else:
				 	include( get_template_directory() . '/views_support/blog/page_numeric/hide_page.php' );
				endif;
			endfor;
		elseif( $page < $last ):
			for($i = 1; $i <= $pages; $i++):
				if( $i == 1 || $i == $pages || ( $i == ( $page - 1 ) ) || ( $i == ( $page + 1 ) ) ):
					include( get_template_directory() . '/views_support/blog/page_numeric/visible_page.php' );
				elseif( $i == 2 || $i == ( $pages - 1 ) ):
					include( get_template_directory() . '/views_support/blog/page_numeric/after_first_page.php' );
				elseif( $i == $page ):
					include( get_template_directory() . '/views_support/blog/page_numeric/active_page.php' );
				else:
					include( get_template_directory() . '/views_support/blog/page_numeric/hide_page.php' );
				endif;
			endfor;
		elseif( $page == $last ):
			for($i = 1; $i <= $pages; $i++):
				if( $i == 1 || $i > $last ):
					include( get_template_directory() . '/views_support/blog/page_numeric/visible_page.php' );
				elseif( $i == 2 ):
					include( get_template_directory() . '/views_support/blog/page_numeric/after_first_page.php' );
				elseif( $i == $last ):
					include( get_template_directory() . '/views_support/blog/page_numeric/active_page.php' );
				else:
					include( get_template_directory() . '/views_support/blog/page_numeric/hide_page.php' );
				endif;
			endfor;
		elseif( $page > $last ):
			for($i = 1; $i <= $pages; $i++):
				if( $i == 1 || ( $i >= $last && $i != $page ) ):
					include( get_template_directory() . '/views_support/blog/page_numeric/visible_page.php' );
				elseif( $i == 2 ):
					include( get_template_directory() . '/views_support/blog/page_numeric/after_first_page.php' );
				elseif( $i == $page ):
					include( get_template_directory() . '/views_support/blog/page_numeric/active_page.php' );
				else:
					include( get_template_directory() . '/views_support/blog/page_numeric/hide_page.php' );
				endif;
			endfor;
		endif;


		if( $page > ($pages - 4) ){
			include( get_template_directory() . '/views_support/blog/page_numeric/arrow/next/deactive.php' );
		}else{
			$i = $page + 3;
			include( get_template_directory() . '/views_support/blog/page_numeric/arrow/next/active.php' );
		}
	elseif( $pages == 1 ):
	else:
		for($i = 1; $i <= $pages; $i++):
			if( $i == $page ):
				include( get_template_directory() . '/views_support/blog/page_numeric/active_page.php' );
			else:
				include( get_template_directory() . '/views_support/blog/page_numeric/visible_page.php' );
			endif;
		endfor;
	endif;
}

function miracle_get_blog_pagenation_string( $page = 1 ){
	$html = '';
	$count_posts = wp_count_posts( 'post' )->publish;
	$ost = $count_posts%1;
	$pages = ( $count_posts - $ost ) / 1 + 0;

	if( $pages > 7 ):

		$last = $pages - 4;

		if( $page < 4 ){
			$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/arrow/prev/deactive.php' );
			$html = str_replace('<?= $i ?>', $i, $html);
		}else{
			$i = $page - 3;
			$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/arrow/prev/active.php' );
			$html = str_replace('<?= $i ?>', $i, $html);
		}

		if( $page < 5 ):
			for($i = 1; $i <= $pages; $i++):
				if( ( $i < 6 && $i != $page ) || $i == $pages ):
					$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/visible_page.php' );
					$html = str_replace('<?= $i ?>', $i, $html);
				elseif( $i == $page ):
				 	$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/active_page.php' );
					$html = str_replace('<?= $i ?>', $i, $html);
				elseif( $i == ( $pages - 1 ) ):
				 	$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/after_first_page.php' );
					$html = str_replace('<?= $i ?>', $i, $html);
				else:
				 	$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/hide_page.php' );
					$html = str_replace('<?= $i ?>', $i, $html);
				endif;
			endfor;
		elseif( $page < $last ):
			for($i = 1; $i <= $pages; $i++):
				if( $i == 1 || $i == $pages || ( $i == ( $page - 1 ) ) || ( $i == ( $page + 1 ) ) ):
					$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/visible_page.php' );
					$html = str_replace('<?= $i ?>', $i, $html);
				elseif( $i == 2 || $i == ( $pages - 1 ) ):
					$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/after_first_page.php' );
					$html = str_replace('<?= $i ?>', $i, $html);
				elseif( $i == $page ):
					$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/active_page.php' );
					$html = str_replace('<?= $i ?>', $i, $html);
				else:
					$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/hide_page.php' );
					$html = str_replace('<?= $i ?>', $i, $html);
				endif;
			endfor;
		elseif( $page == $last ):
			for($i = 1; $i <= $pages; $i++):
				if( $i == 1 || $i > $last ):
					$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/visible_page.php' );
					$html = str_replace('<?= $i ?>', $i, $html);
				elseif( $i == 2 ):
					$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/after_first_page.php' );
					$html = str_replace('<?= $i ?>', $i, $html);
				elseif( $i == $last ):
					$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/active_page.php' );
					$html = str_replace('<?= $i ?>', $i, $html);
				else:
					$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/hide_page.php' );
					$html = str_replace('<?= $i ?>', $i, $html);
				endif;
			endfor;
		elseif( $page > $last ):
			for($i = 1; $i <= $pages; $i++):
				if( $i == 1 || ( $i >= $last && $i != $page ) ):
					$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/visible_page.php' );
					$html = str_replace('<?= $i ?>', $i, $html);
				elseif( $i == 2 ):
					$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/after_first_page.php' );
					$html = str_replace('<?= $i ?>', $i, $html);
				elseif( $i == $page ):
					$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/active_page.php' );
					$html = str_replace('<?= $i ?>', $i, $html);
				else:
					$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/hide_page.php' );
					$html = str_replace('<?= $i ?>', $i, $html);
				endif;
			endfor;
		endif;


		if( $page > ($pages - 4) ){
			$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/arrow/next/deactive.php' );
			$html = str_replace('<?= $i ?>', $i, $html);
		}else{
			$i = $page + 3;
			$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/arrow/next/active.php' );
			$html = str_replace('<?= $i ?>', $i, $html);
		}
	elseif( $pages == 1 ):
	else:
		for($i = 1; $i <= $pages; $i++):
			if( $i == $page ):
				$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/active_page.php' );
				$html = str_replace('<?= $i ?>', $i, $html);
			else:
				$html .= file_get_contents( get_template_directory() . '/views_support/blog/page_numeric/visible_page.php' );
				$html = str_replace('<?= $i ?>', $i, $html);
			endif;
		endfor;
	endif;

	$html = str_replace('<?= $i ?>', $i, $html);
	return $html;
>>>>>>> 5e8d487f7cd07eda030dcb0b1bff29dce422e99f
}