<?php

/**
 *   The register post-type review
 */

add_action('init', 'mis_review_post_init');
function mis_review_post_init(){
	register_post_type('review', array(
		'labels'             => array(
			'name'               => 'Отзыв',
			'singular_name'      => 'Отзыв',
			'add_new'            => 'Новый отзыв',
			'add_new_item'       => 'Добавить новый отзыв',
			'edit_item'          => 'Редактировать отзыв',
			'new_item'           => 'Новый отзыв',
			'view_item'          => 'Посмотреть отзыв',
			'search_items'       => 'Найти отзыв',
			'not_found'          => 'В отзыв не найдено',
			'not_found_in_trash' => 'В корзине не найдено отзыва',
			'parent_item_colon'  => '',
			'menu_name'          => 'Отзывы',
                        'featured_image'     => 'Фото',
                        'set_featured_image' => 'Установить фото',
                        'remove_featured_image' => 'Удалить фото',
                        'use_featured_image' => 'Использовать как фото отзыва',
		  ),
                'description'        => 'Здесь хранятся все отзывы',
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => true,
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => 20,
		'menu_icon'          => 'dashicons-format-status',
		'supports'           => array
					(
					'title',
					'thumbnail',
					'editor'
					)
	) );
}

add_theme_support( 'post-thumbnails', array( 'review' ) );