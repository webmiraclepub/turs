<?php

add_theme_support( 'customize-selective-refresh-widgets' );

add_action('customize_register', function($customizer){
    $customizer->add_panel( 'customizer_global_panel', array(
     'title' => __( 'Общие настройки сайта' ),
     'description' => 'В этом разделе редактируется общие данные для всего сайта',
     'priority' => 1.001,
    ) );


    //////////////////
    //Логотип сайта //
    //////////////////

    $customizer->add_section(
        'customizer_section_logo',
        array(
            'title' => 'Логотип сайта',
            'description' => 'Выберите логотип вашего сайта',
            'panel'       => 'customizer_global_panel'
        )
    );

    $customizer->add_setting('miracle-global-logo');

    $customizer->add_control(
        new WP_Customize_Image_Control(
            $customizer,
            'miracle-global-logo',
            array(
                'label' => '',
                'section' => 'customizer_section_logo',
                'settings' => 'miracle-global-logo'
            )
        )
    );


    /////////////////////
    // Фон шапки сайта //
    /////////////////////

    $customizer->add_section(
        'customizer_section_header_image',
        array(
            'title' => 'Фон шапки сайта',
            'description' => 'Выберите фон, который будет отображаться в шапке Вашего сайта',
            'panel'       => 'customizer_global_panel'
        )
    );

    $customizer->add_setting('miracle-global-header-image');

    $customizer->add_control(
        new WP_Customize_Image_Control(
            $customizer,
            'miracle-global-header-image',
            array(
                'label' => '',
                'section' => 'customizer_section_header_image',
                'settings' => 'miracle-global-header-image'
            )
        )
    );


    ///////////////////////////
    // Заголовок шапки сайта //
    ///////////////////////////

    $customizer->add_section(
        'customizer_section_global_header_title',
        array(
            'title'       => 'Заголовки шапки сайта',
            'description' => 'Заполните заголовки',
            'panel'       => 'customizer_global_panel'
        )
    );
    
    $customizer->add_setting(
      'miracle_global_header_title',
      array('default' => '')
    );

    $customizer->add_control(
      'miracle_global_header_title',
      array(
        'label' => 'Заголовок',
        'section' => 'customizer_section_global_header_title',
        'type' => 'text',
      )
    );
    
    $customizer->add_setting(
      'miracle_global_header_subtitle',
      array('default' => '')
    );

    $customizer->add_control(
      'miracle_global_header_subtitle',
      array(
        'label' => 'Подзаголовок',
        'section' => 'customizer_section_global_header_title',
        'type' => 'text',
      )
    );


    //////////////////////
    // Номера телефонов //
    //////////////////////

    $customizer->add_section(
        'customizer_section_global_phone',
        array(
            'title'       => 'Номера телефонов',
            'description' => 'Заполните все номера телефонов организации',
            'panel'       => 'customizer_global_panel'
        )
    );
    
    $customizer->add_setting(
      'customizer_global_phone_viber',
      array('default' => '')
    );

    $customizer->add_control(
      'customizer_global_phone_viber',
      array(
        'label' => 'Viber',
        'section' => 'customizer_section_global_phone',
        'type' => 'text',
      )
    );
    
    $customizer->add_setting(
      'customizer_global_phone_fenix',
      array('default' => '')
    );

    $customizer->add_control(
      'customizer_global_phone_fenix',
      array(
        'label' => 'Fenix',
        'section' => 'customizer_section_global_phone',
        'type' => 'text',
      )
    );


    ///////////////////////
    // Электронная почта //
    ///////////////////////

    $customizer->add_section(
        'customizer_section_global_email',
        array(
            'title'       => 'Адрес электронной почты',
            'description' => 'Заполните электронный организации',
            'panel'       => 'customizer_global_panel'
        )
    );
    
    $customizer->add_setting(
      'customizer_global_email',
      array('default' => '')
    );

    $customizer->add_control(
      'customizer_global_email',
      array(
        'label' => 'Email',
        'section' => 'customizer_section_global_email',
        'type' => 'text',
      )
    );

    ///////////////////////
    // Адрес организации //
    ///////////////////////

    $customizer->add_section(
        'customizer_section_global_address',
        array(
            'title'       => 'Адрес организации',
            'description' => 'Заполните адрес организации',
            'panel'       => 'customizer_global_panel'
        )
    );
    
    $customizer->add_setting(
      'customizer_global_address',
      array('default' => '')
    );

    $customizer->add_control(
      'customizer_global_address',
      array(
        'label' => 'Адрес компании',
        'section' => 'customizer_section_global_address',
        'type' => 'text',
      )
    );

});
