import $ from 'jquery';

$(document).ready(function(){

    var owl_comment = $('#comment .comment-carousel');
    owl_comment.owlCarousel({
        loop:false,
        margin:10,
        stagePadding: 10,
        nav:true,
        dots:false,
        callbacks:true,
        lazyLoad:true,
        navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
        items:1
    });

    owl_comment.on('changed.owl.carousel', function(event) {
        setTimeout(function(){
            $( "#comment .comment-carousel .owl-stage-outer" ).height( $( "#comment .comment-carousel .owl-item.active" ).height() ), 
            100
        });
    })
});