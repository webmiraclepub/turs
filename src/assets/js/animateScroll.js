import $ from 'jquery';

var animationIn = function() {
	$('[data-animate]').each(function(){
		var scroll_now = $(document).scrollTop();
		var top_position = $(this).offset().top + $(this).height() - 1;
		var bottom_position = $(this).offset().top - screen.height;

		if( scroll_now < top_position && scroll_now > bottom_position && $(this).attr('data-animate') != 'visibility' ){
			var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
			var animationName = $(this).attr('data-animate');
			var d = $(this).attr('data-animate-delay');
			var dr = $(this).attr('data-animate-duration');
			$(this).css({ 'visibility' : 'visible', 'animation-delay' : d, 'animation-duration' : dr}).attr('data-animate', 'visibility');
			$(this).addClass('animated ' + animationName).one(animationEnd, function() {
				$(this).removeClass('animated ' + animationName);
			});
		}
	});
	setTimeout(function(){},10);
}

$(window).on('scroll', animationIn);
$(window).on('load', animationIn);
$(document).ready( function(){
	animationIn;
});