import $ from 'jquery';

$(window).on('scroll', function() {
	
	if( $('[data-number]').length > 0 ){
		var scroll_now = $(document).scrollTop();
		var top_position = $('[data-number]').first().offset().top + $('[data-number]').first().height();
		var top_position2 = $('[data-number]').first().offset().top - screen.height;

		if( scroll_now < top_position && scroll_now > top_position2 && $('[data-number]').first().attr('data-number') != 'visibility' ){
			$('[data-number]').each(
				function(){
					$(this).animateNumber({
							number: $(this).attr('data-number'),
							easing: 'easeInQuad'
						},
						3000
					);
				}
			);
			$('[data-number]').attr('data-number', 'visibility');
			setTimeout(
				function(){
					$('[data-number]').each(
						function(){
							var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
							var animationName = $(this).attr('data-animate-number');
							$(this).addClass('animated ' + animationName).one(animationEnd, function() {
								$(this).removeClass('animated ' + animationName);
							});
						});
				},3000);
		}
	}
	setTimeout(function(){}, 10);
});