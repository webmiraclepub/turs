import $ from 'jquery';

var form_ajax = function(event){
	event.preventDefault();
	var that = $(this);
	var error = false;
	for(var i = 0; i < that.children().length; i++){
	var elem = $(that.children()[i]);
	if (elem.hasClass('form-error is-visible'))
		error = true;
	}
	var name = that.find('input[name="name"]');
	var phone = that.find('input[name="phone"]');;
	var email = that.find('input[name="email"]');;
	var formData = {
		'name' : name,
		'phone' : phone,
		'email' : email
	}
	if (!error){
		$(this).attr('disabled', 'disabled');
		$(this).find('.info').first().html('');
		$(this).find('.info').first().append('Идет отправка сообщения');
		$(this).find('.info').first().removeClass('success alert');
		$(this).find('.info').first().fadeIn();
		$(this).find('.info').first().addClass('primary');
		$.ajax({
			method: "POST",
			url: ajax.url,
			contentType: false,
			processData: false,
			data: formData,
			dataType: 'json',
			success: form_success,
			beforeSend: form_before,
			error: form_error
		});
	}
}

var form_success = function(data){
	$('body form').each(function(index, element){
		if ($(element).attr('disabled') == 'disabled')
			$(element).removeAttr('disabled');
	});
	if( data['answer'] ){
		$('body .info').html('');
		$('body .info').append('Сообщение отправлено успешно');
		$('body .info').removeClass('hidden primary');
		$('body .info').addClass('success');
	}else{
		$('body .info').html('');
		$('body .info').append('Сообщение не отправлено');
		$('body .info').removeClass('hidden primary success');
		$('body .info').addClass('alert');
	}
	setTimeout(function(){ $('body .info').fadeOut() }, 3000);
	// setTimeout(function(){ $('body .info').addClass('hidden') }, 3000);
}

var form_before = function(xhr){
	// $(this).find('.button.orange').first().attr('disabled', 'disabled');
}

var form_error = function(){
 
}

$('form').on( 'submit', form_ajax );