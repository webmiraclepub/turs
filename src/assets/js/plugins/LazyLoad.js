import $ from 'jquery';



$('img[data-src]:not(.owl-lazy)').Lazy({
    effect: 'fadeIn',
    visibleOnly: true,
    onError: function(element) {
        console.log('error loading ' + element.data('src'));
    }
});
                
$('.lazy').lazy();


// $('img').lazyload({
//     skip_invisible : true
// });