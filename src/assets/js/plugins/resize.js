import $ from 'jquery';

$(window).on('load', function(){
  setTimeout( function(){ MIR_resize(); }, 500 );
});

function MIR_resize(){
  $('[data-radio="height"]').each(function(){
    var resize = true;
    if ( screen.width < 769 ){
      if ( $(this).attr( 'data-radio-size' ) == 1.3333333 ) {
        $( this ).attr( 'data-radio-size', 1 );
      }
      if ( ( $( this ).parent().parent().attr( 'class' ) == "item comment-carousel-item" ) && ( $( this ).attr( 'class' ) == "content-text" ) ){
        resize = false;
      }
    }
    if ( resize ){
      var size   = $(this).attr('data-radio-size');
      var width  = $(this).width();
      var height = ( width / size );
      $(this).height( height );
    }
  });
  $('[data-radio="width"]').each(function(){
    var size   = $(this).attr('data-radio-size');
    var height  = $(this).height();
    var width = ( height / size );
    $(this).width( width );
  });
}

$(window).resize(function(){
  MIR_resize();
});