import $ from 'jquery';


$(document).ready(function(){

    $('#tour .tabs-content .owl-carousel.program-carousel').each(function(){
        $(this).owlCarousel({
            margin:0,
            stagePadding:0,
            nav:true,
            dots:false,
            loop:true,
            callbacks:true,
            lazyLoad:true,
            touchDrag: false,
            autoplay: true,
            navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
            items:1
        });
    });

    $('#tour .owl-carousel.tour_gallery_slider').each(function(){
        $(this).owlCarousel({
            margin:0,
            stagePadding:0,
            nav:false,
            dots:true,
            loop:false,
            callbacks:true,
            lazyLoad:true,
            mouseDrag:false,
            autoplay:true,
            touchDrag: true,
            animateIn:'fadeIn',
            animateOut:'fadeOut',
            items:1
        });
    });

});

$('#tour .tabs-content .owl-carousel').each(function(){
	$(this).on( 'translated.owl.carousel', function(event) {
		var day     = $(this).find('.owl-item.active').find('.item').attr('data-tour-day');
		var content = $(this).find('.owl-item.active').find('.item').attr('data-tour-content');
        var content = ( content != undefined ) ? content.replace(new RegExp("''", "g" ), '"') : content;
		$(this).next().find('p.day').fadeOut(400, function(){
			$(this).html(day+'</br>день');
			$(this).fadeIn();
		});
		$(this).next().find('p.content').fadeOut(400, function(){
			$(this).html(content);
			$(this).fadeIn();
		});
	});
});

$('[data-toggler=".hide bounceInUp"]').each(function(){
    $(this).on( 'off.zf.toggler', function(){
        $(this).find('[data-radio-size]').each(function( index, element ){
            setTimeout( function(){
                var size   = $(element).attr('data-radio-size');
                var width  = $(element).width();
                var height = ( width / size );
                $(element).height( height );
            }, 500 );
        });
    });
});