import $ from 'jquery';
import Foundation from 'foundation-sites';

$(document).ready(function(){
	if( $( 'section#tour #example-tabs .tabs-title' ).length > 0 ){
		
		var tour_ajax_success = function(data){
				$('#tour .after-tabs p').fadeOut(400, function(){
					if( data == 0 ){
						$('#tour .after-tabs p').html( 'доступных туров на данный момент не организовано' );
						$('#tour .after-tabs p').fadeIn();
					}else if( data == 1 ){
						$('#tour .after-tabs p').html( 'доступен ' + data + ' тур' );
						$('#tour .after-tabs p').fadeIn();
					}else if( data > 1 && data < 5 ){
						$('#tour .after-tabs p').html( 'доступно ' + data + ' тура' );
						$('#tour .after-tabs p').fadeIn();
					}else{
						$('#tour .after-tabs p').html( 'доступно ' + data + ' туров' );
						$('#tour .after-tabs p').fadeIn();
					}
				});
			}

		$( 'section#tour #example-tabs .tabs-title a' ).on( 'click', function(){
			// active_click();
			if( !$(this).parent().hasClass( 'is-active' ) ){
				var text = $(this).attr('href');
				switch( text ){
					case '#autumn_tours':
						var data = $('#autumn_tours').attr('data-count');
						if( data == 0 ){
							$('#tour .after-tabs p').fadeOut(400, function(){
								$('#tour .after-tabs p').html( 'доступных туров на данный момент не организовано' );
								$('#tour .after-tabs p').fadeIn();
							});
						}else if( data == 1 ){
							$('#tour .after-tabs p').fadeOut(400, function(){
								$('#tour .after-tabs p').html( 'доступен ' + data + ' тур' );
								$('#tour .after-tabs p').fadeIn();
							});
						}else if( data > 1 && data < 5 ){
							$('#tour .after-tabs p').fadeOut(400, function(){
								$('#tour .after-tabs p').html( 'доступно ' + data + ' тура' );
								$('#tour .after-tabs p').fadeIn();
							});
						}else{
							$('#tour .after-tabs p').fadeOut(400, function(){
								$('#tour .after-tabs p').html( 'доступно ' + data + ' туров' );
								$('#tour .after-tabs p').fadeIn();
							});
						}
						var n = 'сентябрь/октябрь/ноябрь';
						$('#tour .after-tabs h2').fadeOut(400, function(){
							$('#tour .after-tabs h2').html( n );
							$('#tour .after-tabs h2').fadeIn();
						});
						break;
					case '#winter_tours':
						var data = $('#winter_tours').attr('data-count');
						if( data == 0 ){
							$('#tour .after-tabs p').fadeOut(400, function(){
								$('#tour .after-tabs p').html( 'доступных туров на данный момент не организовано' );
								$('#tour .after-tabs p').fadeIn();
							});
						}else if( data == 1 ){
							$('#tour .after-tabs p').fadeOut(400, function(){
								$('#tour .after-tabs p').html( 'доступен ' + data + ' тур' );
								$('#tour .after-tabs p').fadeIn();
							});
						}else if( data > 1 && data < 5 ){
							$('#tour .after-tabs p').fadeOut(400, function(){
								$('#tour .after-tabs p').html( 'доступно ' + data + ' тура' );
								$('#tour .after-tabs p').fadeIn();
							});
						}else{
							$('#tour .after-tabs p').fadeOut(400, function(){
								$('#tour .after-tabs p').html( 'доступно ' + data + ' туров' );
								$('#tour .after-tabs p').fadeIn();
							});
						}
						var n = 'Декабрь/Январь/февраль';
						$('#tour .after-tabs h2').fadeOut(400, function(){
							$('#tour .after-tabs h2').html( n );
							$('#tour .after-tabs h2').fadeIn();
						});
						break;
					case '#spring_tours':
						var data = $('#spring_tours').attr('data-count');
						if( data == 0 ){
							$('#tour .after-tabs p').fadeOut(400, function(){
								$('#tour .after-tabs p').html( 'доступных туров на данный момент не организовано' );
								$('#tour .after-tabs p').fadeIn();
							});
						}else if( data == 1 ){
							$('#tour .after-tabs p').fadeOut(400, function(){
								$('#tour .after-tabs p').html( 'доступен ' + data + ' тур' );
								$('#tour .after-tabs p').fadeIn();
							});
						}else if( data > 1 && data < 5 ){
							$('#tour .after-tabs p').fadeOut(400, function(){
								$('#tour .after-tabs p').html( 'доступно ' + data + ' тура' );
								$('#tour .after-tabs p').fadeIn();
							});
						}else{
							$('#tour .after-tabs p').fadeOut(400, function(){
								$('#tour .after-tabs p').html( 'доступно ' + data + ' туров' );
								$('#tour .after-tabs p').fadeIn();
							});
						}
						var n = 'Март/апрель/май';
						$('#tour .after-tabs h2').fadeOut(400, function(){
							$('#tour .after-tabs h2').html( n );
							$('#tour .after-tabs h2').fadeIn();
						});
						break;
					case '#summer_tours':
						var data = $('#summer_tours').attr('data-count');
						if( data == 0 ){
							$('#tour .after-tabs p').fadeOut(400, function(){
								$('#tour .after-tabs p').html( 'доступных туров на данный момент не организовано' );
								$('#tour .after-tabs p').fadeIn();
							});
						}else if( data == 1 ){
							$('#tour .after-tabs p').fadeOut(400, function(){
								$('#tour .after-tabs p').html( 'доступен ' + data + ' тур' );
								$('#tour .after-tabs p').fadeIn();
							});
						}else if( data > 1 && data < 5 ){
							$('#tour .after-tabs p').fadeOut(400, function(){
								$('#tour .after-tabs p').html( 'доступно ' + data + ' тура' );
								$('#tour .after-tabs p').fadeIn();
							});
						}else{
							$('#tour .after-tabs p').fadeOut(400, function(){
								$('#tour .after-tabs p').html( 'доступно ' + data + ' туров' );
								$('#tour .after-tabs p').fadeIn();
							});
						}

						var n = 'июнь/июль/август';
						$('#tour .after-tabs h2').fadeOut(400, function(){
							$('#tour .after-tabs h2').html( n );
							$('#tour .after-tabs h2').fadeIn();
						});
						break;
					default:
						break;
				}
			}
		});
		$('#example-tabs').foundation().on('change.zf.tabs', function(){
			var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
			var animationName = 'bounceIn';
			$('[data-tabs-content="example-tabs"]').find('div.is-active').css('animation-duration', '0.4s');
			$('[data-tabs-content="example-tabs"]').find('div.is-active').addClass('animated ' + animationName).one(animationEnd, function() {
				$('[data-tabs-content="example-tabs"]').find('div.is-active').removeClass('animated ' + animationName);
			});
			$('[data-toggler]').each(function(index,item){
				$(item).removeClass('bounceInUp').addClass('hide');
			});
			active_click();
		});
	}
});

function active_click(){
	setTimeout(function(){
		$('.active_tour').each(function(){
			if($(this).parent().parent().parent().parent().parent().hasClass('is-active')){
				$( '#' + $(this).attr( 'data-toggle' ) ).removeClass( 'hide' );
				$( '#' + $(this).attr( 'data-toggle' ) ).addClass( 'bounceInUp' );

				var id = $(this).attr( 'data-toggle' );
				var id = ( id != undefined ) ? id.substr(-3) : '';
				$( '#' + $(this).attr( 'data-toggle' ) ).find('[data-radio-size]').each(function( index, element ){
					setTimeout( function(){
						var size   = $(element).attr('data-radio-size');
						var width  = $(element).width();
						var height = ( width / size );
						$(element).height( height );

						var obj = $('[data-equalizer="owl-carousel-' + id + '"]');
						if( obj.length > 0 ){
							Foundation.reInit( obj );
						}
					}, 1000 );
				});
			}
		});
	},1000)
}