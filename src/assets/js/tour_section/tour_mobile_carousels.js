import $ from 'jquery';


$(document).ready(function(){

    var owl_win = $('#owl-winter').owlCarousel({
        margin:0,
        stagePadding:0,
        nav:false,
        dots:false,
        loop:true,
        callbacks:true,
        touchDrag: false,
        lazyLoad:true,
        autoplay: true,
        touchDrag: true,
        // autoHeight: true,
        autoplayTimeout: 7000, 
        navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
        items:1
    });

    var owl_sum = $('#owl-summer').owlCarousel({
        margin:0,
        stagePadding:0,
        nav:false,
        dots:false,
        loop:true,
        callbacks:true,
        lazyLoad:true,
        autoplay: true,
        touchDrag: true,
        // autoHeight: true,
        autoplayTimeout: 7000,
        navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
        items:1
    });

    var owl_aut = $('#owl-autumn').owlCarousel({
        margin:0,
        stagePadding:0,
        nav:false,
        dots:false,
        loop:true,
        callbacks:true,
        lazyLoad:true,
        autoplay: true,
        touchDrag: true,
        autoplayTimeout: 7000,
        // autoHeight: true,
        navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
        items:1
    });

    var owl_spr = $('#owl-spring').owlCarousel({
        margin:0,
        stagePadding:0,
        nav:false,
        dots:false,
        loop:true,
        callbacks:true,
        lazyLoad:true,
        autoplay: true,
        touchDrag: true,
        autoplayTimeout: 7000,
        // autoHeight: true, 
        navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
        items:1
    });

});