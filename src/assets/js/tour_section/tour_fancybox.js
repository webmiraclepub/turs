import $ from 'jquery';

$(document).ready(function() {
	if( $('#tour').length > 0 ){
		$("#tour .fancybox-thumb").fancybox({
			helpers	: {
				thumbs	: {
					width	: 50,
					height	: 50
				}
			}
		});
	}
});