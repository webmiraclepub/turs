import $ from 'jquery';
import whatInput from 'what-input';

window.$ = $;

import Foundation from 'foundation-sites';
// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
//import './lib/foundation-explicit-pieces';

//import './lib/demosite';
import './plugins/jquery.lazy';
import './plugins/jquery.lazy.plugins';
import './plugins/jquery.fancybox';
import './plugins/LazyLoad';
import './plugins/owl.carousel';
import './plugins/animateNumber';
import './plugins/resize';

import './about_us_item';
import './logo_animate';
import './animateScroll';
import './working_carousel';
import './reveiw_carousel';
import './comment_carousel';

import './tour_section/tour_fancybox';
import './tour_section/tabs_click';
import './tour_section/tour_info_carousel';
import './tour_section/tour_mobile_carousels';

import './ajax/gallery_posts';
import './ajax/blog';
import './ajax/mail.js';
import './edits';


$(document).foundation();