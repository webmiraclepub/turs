import $ from 'jquery';
import Foundation from 'foundation-sites';

$(document).ready(function(){
  $('#working .working-carousel').owlCarousel({
    margin:10,
    stagePadding: 10,
    nav:true,
    dots:false,
    touchDrag: true,
    callbacks :  true,
    lazyLoad:true,
    navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
    responsive:{
        0:{
            items:1
        },
        800:{
            items:2
        },
        1000:{
            items:3
        }
    }
    });
}); 

var equalizer_reload = function(){
    if( $('#working .grid-working[data-miracle-equalizer]').length > 0 ){
        var obj1 = $('#working .grid-working');
        var obj2 = $('#working .working-carousel');
        var e1 = obj1.attr( 'data-miracle-equalizer' );
        var e2 = obj2.attr( 'data-miracle-equalizer' );
        var scroll_now = $(document).scrollTop();
        var top_position = obj1.offset().top + obj1.height() - 1;
        var bottom_position = obj1.offset().top - screen.height;

        if( scroll_now < top_position && scroll_now > bottom_position ){
            obj1.removeAttr( 'data-miracle-equalizer' ).attr( 'data-equalizer', e1 ).find('[data-miracle-equalizer-watch="'+e1+'"]').each(function(index,elem){
                $(elem).removeAttr( 'data-miracle-equalizer-watch' ).attr( 'data-equalizer-watch', e1 );
            });
            obj2.removeAttr( 'data-miracle-equalizer' ).attr( 'data-equalizer', e2 ).find('[data-miracle-equalizer-watch="'+e2+'"]').each(function(index,elem){
                $(elem).removeAttr( 'data-miracle-equalizer-watch' ).attr( 'data-equalizer-watch', e2 );
            });

            var equa1 = new Foundation.Equalizer( obj1 );
            var equa2 = new Foundation.Equalizer( obj2 );
        }
        setTimeout( function(){}, 10 );
    }
}

// $(window).on('scroll', equalizer_reload);