import $ from 'jquery';

$(document).ready(function(){
    $('#reveiw .reveiw-carousel').owlCarousel({
        items:1,
        merge:true,
        loop:false,
        margin:10,
        video:true,
        videoWidth: 500,
        videoHeight: 350,
        lazyLoad:true,
        center:true,
        dots:false,
        nav:false,
        responsive:{
            320:{
                nav: false,
                videoWidth: 320,
                videoHeight: 350,
            },
            600:{
                nav: false,
                videoWidth: 500,
                videoHeight: 350,
            },
            1000:{
                nav: true,
            }
        },
        navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>']
    });
});

$('#reveiw .reveiw-carousel').on( 'initialize.owl.carousel' ,function(event) {
    $('#reveiw .reveiw-carousel').trigger('refresh.owl.carousel');
});

$('#reveiw .reveiw-carousel').on( 'refreshed.owl.carousel' ,function(event) {
    setTimeout( function(){ $('#reveiw .reveiw-carousel').trigger('refresh.owl.carousel'); }, 1000);
});