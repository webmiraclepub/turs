import $ from 'jquery';


var logoIn = function() {

	var scroll_now = $(document).scrollTop();
	var top_position = -1;
	var bottom_position = 100;

	if( scroll_now > top_position && scroll_now < bottom_position && $('#site-logo').first().attr('data-animate-logo') != 'visibility' ){
		$('#site-logo').css('visibility','visible').attr('data-animate-logo', 'visibility');
		var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
		var animationName = 'bounceInDown';
		$('#site-logo').addClass('animated ' + animationName).one(animationEnd, function() {
			$('#site-logo').removeClass('animated ' + animationName);
		});
	}
	setTimeout(function(){}, 10);
}

window.onscroll = logoIn;
$(window).on('load', logoIn);
$(document).ready( function(){
	logoIn;
});