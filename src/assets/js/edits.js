import $ from 'jquery';

$(window).on('load', active_click);

// $('#winter_tours-label').bind('click',active_click);
// $('#autumn_tours-label').bind('click',active_click);
// $('#summer_tours-label').bind('click',active_click);
// $('#spring_tours-label').bind('click',active_click);

function active_click(){
	setTimeout(function(){
		$('.active_tour').each(function(){
			if($(this).parent().parent().parent().parent().parent().hasClass('is-active')){
				$( '#' + $(this).attr( 'data-toggle' ) ).removeClass( 'hide' );
				$( '#' + $(this).attr( 'data-toggle' ) ).addClass( 'bounceInUp' );

				$( '#' + $(this).attr( 'data-toggle' ) ).find('[data-radio-size]').each(function( index, element ){
					setTimeout( function(){
						var size   = $(element).attr('data-radio-size');
						var width  = $(element).width();
						var height = ( width / size );
						$(element).height( height );
					}, 1000 );
				});
			}
		});
	},1000)
}
