import $ from 'jquery';

var form_ajax = function(ev, frm){

	var that = $(this);
	var error = false;
	for(var i = 0; i < that.children().length; i++){
	var elem = $(that.children()[i]);
	if (elem.hasClass('form-error is-visible'))
		error = true;
	}
	var name = $(frm).find('input[name="name"]').first().val();
	var phone = $(frm).find('input[name="phone"]').first().val();
	var email = $(frm).find('input[name="email"]').first().val();
	var formData = {
		'name' : name,
		'phone' : phone,
		'email' : email,
		'action' : 'miracle_mail',
		"nonce"   : ajax.nonce
	}
	try{
		var tour = $(frm).find('input[name="tour"]').first().val();
		formData['tour'] = tour;
	}catch(e){}
	if (!error){
		$(frm).attr('disabled', 'disabled');
		$(frm).find('.info').first().html('');
		$(frm).find('.info').first().append('Идет отправка сообщения');
		$(frm).find('.info').first().removeClass('success alert');
		$(frm).find('.info').first().fadeIn();
		$(frm).find('.info').first().addClass('primary');
		$.ajax({
			method: "POST",
			url: ajax.url,
			data: formData,
			success: form_success,
			beforeSend: form_before,
			error: form_error
		});
	}
}

var form_success = function(data){
	var data = $.parseJSON(data);
	$('body form').each(function(index, element){
		if ($(element).attr('disabled') == 'disabled')
			$(element).removeAttr('disabled');
	});
	if( data['answer'] ){
		$('body .info').html('');
		$('body .info').append('Сообщение отправлено успешно');
		$('body .info').removeClass('hidden primary');
		$('body .info').addClass('success');
	}else{
		$('body .info').html('');
		$('body .info').append('Сообщение не отправлено');
		$('body .info').removeClass('hidden primary success');
		$('body .info').addClass('alert');
	}
	setTimeout(function(){ $('body .info').fadeOut() }, 3000);
}

var form_before = function(xhr){
}

var form_error = function(){
	alert('error');
}

$(document).on("formvalid.zf.abide", form_ajax);
 
$(document).on("submit", function(ev) {
    ev.preventDefault();
});