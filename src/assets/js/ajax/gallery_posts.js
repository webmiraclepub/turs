import $ from 'jquery';

$(document).ready( function(){
	var count = $('#photo-gallery-card .grid-x.gallery').length;
	if( count < 3 ){
		$('#add_new_gallery_posts').addClass('hide');
	}
});

var ajax_gallery = function(){
	var count = $('#photo-gallery-card .grid-x.gallery').length;
	$.ajax({
		method: "POST",
		url: ajax.url,
		data: {
			"count"   : count,
			"action"  : 'miracle_get_new_gallery_posts',
			"id"      : ajax.i,
			"nonce"   : ajax.nonce
		},
		success: gallery_ajax_success,
		beforeSend: gallery_ajax_before,
		error: gallery_ajax_error
	});
}

var gallery_ajax_success = function(data){
	var html = $( data );
	$('#photo-gallery-card .grid-x.gallery').first().parent().append( html );
	$('#photo-gallery-card .grid-x.gallery').find('[data-radio="height"]').each(function(){
		var size   = $(this).attr('data-radio-size');
		var width  = $(this).width();
		var height = ( width / size );
		$(this).height( height );
	});

	$('#photo-gallery-card').find('[data-animate]').each(function(){
		if( $(this).attr('data-animate') != 'visibility' ){
			var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
			var animationName = $(this).attr('data-animate');
			var d = $(this).attr('data-animate-delay');
			var dr = $(this).attr('data-animate-duration');
			$(this).css({ 'visibility' : 'visible', 'animation-delay' : d, 'animation-duration' : dr}).attr('data-animate', 'visibility');
			$(this).addClass('animated ' + animationName).one(animationEnd, function() {
				$(this).removeClass('animated ' + animationName);
			});
		}
	});

	$('#add_new_gallery_posts').removeAttr('disabled');
	var count = html.length;
	if( count != 3 ){
		console.log( html );
		$('#add_new_gallery_posts').fadeOut(300, function(){
			$('#add_new_gallery_posts').addClass('hide');
		});
	}
}

var gallery_ajax_before = function(){
	$('#add_new_gallery_posts').attr('disabled', 'disabled');
}

var gallery_ajax_error = function(){
	alert('Sorry. The request has not passed, maybe you violated the rules of using the site');
	$('#add_new_gallery_posts').addClass('hide');
}

$('#add_new_gallery_posts').on( 'click', ajax_gallery );