import $ from 'jquery';

var blog_ajax = function(event){
	var page = $(this).attr('data-page');
	$.ajax({
		method: "POST",
		url: ajax.url,
		data: {
			"page"    : page,
			"action"  : 'miracle_blog_page',
			"type"    : 'post',
			"nonce"   : ajax.nonce
		},
		success: blog_success,
		beforeSend: blog_before,
		error: blog_error
	});
}

var blog_success = function(data){
	// console.log( data );
	var data = $.parseJSON(data);
	var html = $( data.posts );
	var pagination = $( data.pages );

	$('#blog .post[data-animate]').each(function(index){
		var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
		var animationName = ( index%2 == 0 ) ? 'bounceOutLeft':'bounceOutRight';
		var d = '0s';
		var dr = '1.5s';
		$(this).css({ 'visibility' : 'visible', 'animation-delay' : d, 'animation-duration' : dr}).attr('data-animate', 'visibility');
		$(this).addClass('animated ' + animationName).one(animationEnd, function() {
			$(this).remove();
		});
	});

	$('#blog .all-posts .grid-x').append( html );
	$('#blog .all-posts .grid-x').find('[data-radio="height"]').each(function(){
		var size   = $(this).attr('data-radio-size');
		var width  = $(this).width();
		var height = ( width / size );
		$(this).height( height );
	});

	$('#blog .all-posts .grid-x').find('[data-animate]').each(function(){
		if( $(this).attr('data-animate') != 'visibility' ){
			var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
			var animationName = $(this).attr('data-animate');
			var d = '1.5s';
			var dr = '1.5s';
			$(this).css({ 'visibility' : 'visible', 'animation-delay' : d, 'animation-duration' : dr}).attr('data-animate', 'visibility');
			$(this).addClass('animated ' + animationName).one(animationEnd, function() {
				$(this).removeClass('animated ' + animationName);
			});
		}
	});

	$('#blog .pagination').html(pagination);

	$('#blog .pagination').fadeIn();
	$('#blog .pagination [data-page]').bind( 'click', blog_ajax );
}

var blog_before = function(){
	$('#blog .pagination').fadeOut();
}

var blog_error = function(){
	alert('Sorry. The request has not passed, maybe you violated the rules of using the site');
	$('#blog .pagination').fadeIn();
}

$('#blog .pagination [data-page]').bind( 'click', blog_ajax );