<?php
  $logo = get_field( 'miracle_global_site_logo', 'option' );
  $logo_img = $logo['url'];
  $logo_alt = $logo['alt'];
  $logo_title = $logo['description'];
  $viber_phone = get_field('miracle_option_phone_viber', 'option');
  $fenix_phone = get_field('miracle_option_phone_fenix', 'option');
  $other_phone = '';
  foreach ( get_field('miracle_option_other_phone', 'option') as $phone) {
    $other_phone .= '<br><a href="callto:'.$phone['phone'].'">'.$phone['phone'].'</a>';
  }
  $email = get_field('miracle_option_site_email', 'option');
  $address = get_field('miracle_option_site_address', 'option');
?>

<?php miracle_get_before_footer(); ?>

<footer id="footer">
  <div id="bottom-logo" class="grid-container">
    <div class="grid-x">
      <div class="for-logo">
        <div class="cell">
          <img id="site-logo" data-animate="bounceIn" data-animate-delay="0.5s" data-animate-duration="1s" data-src="<?= $logo_img ?>" alt="<?= $logo_alt ?>" title="<?= $logo_title ?>">
        </div>
      </div>
      <div class="footer-phone" data-animate="fadeInLeft" data-animate-delay="0.5s" data-animate-duration="1s">
        <i class="miracle-icon-phone hide-for-small-only"></i>
        <p><a href="callto:<?= $viber_phone ?>"><?= $viber_phone ?></a><br><a href="callto:<?= $fenix_phone ?>"><?= $fenix_phone ?></a><?= $other_phone ?></p>
      </div>
      <div class="footer-address" data-animate="fadeInRight" data-animate-delay="0.5s" data-animate-duration="1s">
        <p><?= $email ?><i class="miracle-icon-mail"></i></p>
        <p><?= $address ?><i class="miracle-icon-map"></i></p>
        <ul class="menu align-right">
          <?php miracle_get_social_site(); ?>
        </ul>
      </div>
    </div>
  </div>
</footer>