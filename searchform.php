<?php $unique_id = esc_attr( uniqid( 'search-form-' ) ); ?>

<div class="grid-container">
	<div class="grid-x grid-padding-x">
		<div 
			class="small-12 cell" 
			data-animate="jackInTheBox"
			data-animate-delay="1s"
			data-animate-duration="1s">
			<form role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
				<label class="input" for="<?php echo $unique_id; ?>">
					<input type="search" name="s" value="<?php echo get_search_query(); ?>" placeholder="Что Вы ищете?" />
				</label>
			</form>
		</div>
	</div>
</div>